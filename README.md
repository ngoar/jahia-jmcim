Jahia MailChimp Integration
===========================

This module implements Jahia integration with MailChimp - popular newsletter management systems.

License
-------
The source code is available under the Apache License V2.

Requirements
------------
This module works with Jahia Digital Factory 7.1.2.0 and later.

Main features
-------------
- Share some site content as MailChimp Campaign. Campaigns may be created and sent manually or Cron based. All campaigns  
- Use site content as MailChimp Templates.
- Integrate MailChimp lists subscription into Jahia based site using subscribe component. Quick subscribe and unsubscribe for logged in users supported.     
- Basic statistics dashboard for selected lists and campaigns.


Before started
--------------
Before using this module, you need a MailChimp account (http://mailchimp.com) and MailChimp API key (https://us10.admin.mailchimp.com/account/api/ ).

Usage
-----
User manual
https://bitbucket.org/ngoar/jahia-jmcim/raw/HEAD/Getting%20started%20with%20Jahia%20MailChimp%20Integration%20module.docx
Demo video of this module.
https://www.youtube.com/watch?v=TGSatsH705A

First of all, you need to deploy the module on your Jahia Server.
And then just choose this module to deploy it to your site (in Administration mode).
MailChimp configuration panel will be available in Site Settings panel.

To make this module work on your website you need to put your MailChimp API key to MailChimp Configuration in Settings panel.
All other panels (Campaigns, Templates and Lists) will be available, after  API key validation.

Campaigns
---------
Use this panel, to create and start MailChimp Campaigns.
While creating campaign you can specify:

    - scheduler expression (Cron), for auto send campaigns in schedule period;
    - number of days, for delete campaign that was created older than this period;
    - |MERGETAGS|, for recipient name;
    - and other necessary parameters.


Actions available for each campaign are:
* Edit
    
       Modify name and other necessary parameters for the current campaign.
    
* Go to Edit mode
    
       Show page (content) selected for the current campaign.
    
* Edit in MailChimp
    
       Redirect to this campaign in MailChimp site.
    
* Update Content
    
       Refresh page (content) for this campaign in MailChimp.
    
* Remove
    
      Completely delete this campaign.
        
    
Templates
---------
In this panel, you can create email templates for MailChimp from existing pages/content of the site.

Actions available for template are:

* Edit
    
       Modify name and content node for editing template.
    
* Go to Edit mode
    
       Show the page (content) selected for the email template.
    
* Edit in MailChimp
    
       Redirect to this template in MailChimp site.
    
* Update Content
    
       Refresh page (content) for this template in MailChimp.
    
* Remove
    
      Completely delete this email template.
    
    
Lists
-----
In this panel, you can load recipient lists from MailChimp.
After loading selected list, you can see its statistics (Rating, Members, Unsubscribe, Cleaned).

Actions available for each  list are:

* Edit
    
       Editing configurations of list (message on automatic subscribe, Merge Vars and Webhook)
    
* Edit in MailChimp
    
       Redirect to this list in MailChimp site.
    
* Remove
    
      Delete this list from Jahia MailChimp Configuration only. In MailChimp, this list will be available. And this list will be available for repeated adding in configuration.
    
* Create MailChimp Lists
    
       Redirect to  Create List page in MailChimp site. (We can create members list only in MailChimp)
    
        
MailChimp subscribe component
-----------------------------
This component available in Site Components in edit mode. 
For adding the subscription to the site page just add this component to the page.
After that, the user can subscribe and unsubscribe from the mailing list clicking on this component.

 MailChimp® is a registered trademark of The Rocket Science Group.