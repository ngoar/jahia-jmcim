/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings.model;

import org.jahia.services.content.JCRNodeWrapper;

import javax.jcr.RepositoryException;
import java.io.Serializable;

/**
 * Created by Alexander Storozhuk on 07.05.2015.
 * Time: 14:58
 */
public class CampaignSetModel implements Serializable {
    String campaignSetUUID;
    String title;
    String type;
    String subject;
    String fromName;
    String fromEmail;
    String toName="*|FNAME|*";
    String contentNode;
    String contentNodeDecoy;
    String listId;
    String templateCampId;
    String scheduler;
    Long cleanPeriod;
    boolean autoSend = true;
    boolean isWasSend = false;
    boolean isUpdate = false;

    public CampaignSetModel(JCRNodeWrapper node) throws RepositoryException {
        setCampaignSetUUID(node.getIdentifier());
        setTitle(node.getPropertyAsString("jcr:title"));
        setType(node.getPropertyAsString("type"));
        setSubject(node.getPropertyAsString("subject"));
        setFromEmail(node.getPropertyAsString("fromEmail"));
        setFromName(node.getPropertyAsString("fromName"));
        setToName(node.getPropertyAsString("toName"));
        JCRNodeWrapper contentNode = (JCRNodeWrapper) node.getProperty("contentNode").getNode();
        setContentNode(contentNode.getIdentifier());
        setContentNodeDecoy(contentNode.getDisplayableName());
        setListId(node.getProperty("list").getNode().getIdentifier());
        setTemplateCampId(node.getPropertyAsString("templateCampaignId"));
        setScheduler(node.getPropertyAsString("scheduler"));
        setAutoSend(node.getPropertyAsString("autoSend").equals("true"));
        if (node.hasProperty("cleanPeriod"))
            setCleanPeriod(node.getProperty("cleanPeriod").getLong());
        if(null!=node.getProperty("wasSend"))
            setWasSend(node.getProperty("wasSend").getBoolean());
        setUpdate(true);
    }


    public CampaignSetModel () {
    }

    public String getCampaignSetUUID() {
        return campaignSetUUID;
    }

    public void setCampaignSetUUID(String campaignSetUUID) {
        this.campaignSetUUID = campaignSetUUID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getContentNode() {
        return contentNode;
    }

    public void setContentNode(String contentNode) {
        this.contentNode = contentNode;
    }

    public String getContentNodeDecoy() {
        return contentNodeDecoy;
    }

    public void setContentNodeDecoy(String contentNodeDecoy) {
        this.contentNodeDecoy = contentNodeDecoy;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getTemplateCampId() {
        return templateCampId;
    }

    public void setTemplateCampId(String templateCampId) {
        this.templateCampId = templateCampId;
    }

    public String getScheduler() {
        return scheduler;
    }

    public void setScheduler(String scheduler) {
        this.scheduler = scheduler;
    }

    public Long getCleanPeriod() {
        return cleanPeriod;
    }

    public void setCleanPeriod(Long cleanPeriod) {
        this.cleanPeriod = Math.abs(cleanPeriod);
    }

    public String getCleanPeriodStr() {
        return cleanPeriod==null?null:cleanPeriod.toString();
    }

    public void setCleanPeriodStr(String cleanPeriod) {
        this.cleanPeriod = cleanPeriod==null || cleanPeriod.trim().isEmpty()?null: Math.abs(Long.parseLong(cleanPeriod));
    }

    public boolean isAutoSend() {
        return autoSend;
    }

    public void setAutoSend(boolean autoSend) {
        this.autoSend = autoSend;
    }

    public boolean isWasSend() {
        return isWasSend;
    }

    public void setWasSend(boolean wasSend) {
        isWasSend = wasSend;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }
}
