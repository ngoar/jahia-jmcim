/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings.model;

import org.jahia.modules.mailchimp.api.lists.WebHookResult;

import java.io.Serializable;

/**
 * Created by naf on 11.05.2015.
 */
public class WebHookModel implements Serializable {

    public static final int ACTION_SUBSCRIBE=0;
    public static final int ACTION_UNSUBSCRIBE=1;
    public static final int ACTION_PROFILE=2;
    public static final int ACTION_CLEANED=3;
    public static final int ACTION_UPEMAIL=4;
    public static final int ACTION_CAMPAIGN=5;
    public static final int SOURCE_USER=0;
    public static final int SOURCE_ADMIN=1;
    public static final int SOURCE_API=2;
    private String url;
    private boolean [] actions = new boolean[6];
    private boolean [] sources = new boolean[4];
    private boolean special=false;
    private String actionsStr;
    private String actionsHint;
    private String sourcesStr;
    private String sourcesHint;

    public WebHookModel(WebHookResult webhook) {
        url=webhook.url;
        StringBuilder sb=new StringBuilder("");
        StringBuilder sbHint=new StringBuilder("");
        actions[ACTION_SUBSCRIBE]=webhook.actions.subscribe;
        actions[ACTION_UNSUBSCRIBE]=webhook.actions.unsubscribe;
        actions[ACTION_PROFILE]=webhook.actions.profile;
        actions[ACTION_CLEANED]=webhook.actions.cleaned;
        actions[ACTION_UPEMAIL]=webhook.actions.upemail;
        actions[ACTION_CAMPAIGN]=webhook.actions.campaign;
        appendHint(webhook.actions.subscribe,"S","subscribes",sb,sbHint);
        appendHint(webhook.actions.unsubscribe,"U","unsubscribes",sb,sbHint);
        appendHint(webhook.actions.profile,"P","profile updates",sb,sbHint);
        appendHint(webhook.actions.cleaned,"C","cleaned address",sb,sbHint);
        appendHint(webhook.actions.upemail,"E","email changed",sb,sbHint);
        appendHint(webhook.actions.campaign,"D","campaign sending",sb,sbHint);

        actionsStr=sb.toString();
        actionsHint=sbHint.toString();
        sb.setLength(0);
        sbHint.setLength(0);
        sources[SOURCE_USER]=webhook.sources.user;
        sources[SOURCE_ADMIN]=webhook.sources.admin;
        sources[SOURCE_API]=webhook.sources.api;
        appendHint(webhook.sources.user,"U","subscriber",sb,sbHint);
        appendHint(webhook.sources.admin,"A","account admin",sb,sbHint);
        appendHint(webhook.sources.api,"I","API",sb,sbHint);
        sourcesStr=sb.toString();
        sourcesHint =sbHint.toString();
    }

    private void appendHint(Boolean val, String action, String hint, StringBuilder actionBuff, StringBuilder hintBuff) {
        if (val) {
            actionBuff.append(action);
            if (hintBuff.length()>0)
                hintBuff.append(", ");
            hintBuff.append(hint);
        }

    }
    public String getUrl() {
        return url;
    }

    public boolean[] getActions() {
        return actions;
    }

    public boolean[] getSources() {
        return sources;
    }

    public boolean isSpecial() {
        return special;
    }

    public String getActionsStr() {
        return actionsStr;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public String getSourcesStr() {
        return sourcesStr;
    }

    public String getActionsHint() {
        return actionsHint;
    }

    public String getSourcesHint() {
        return sourcesHint;
    }
}
