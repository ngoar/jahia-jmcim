/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings;

import com.ecwid.mailchimp.MailChimpException;
import org.jahia.modules.mailchimp.service.TemplateService;
import org.jahia.modules.mailchimp.sitesettings.model.TemplateModel;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.jahia.utils.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.webflow.execution.RequestContext;

import javax.jcr.RepositoryException;
import java.io.Serializable;


/**
 * Created by Alexander Storozhuk on 03.04.2015.
 * Time: 19:36
 */
public class ManageTemplateFlowHandler implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ManageTemplateFlowHandler.class);
    private static final long serialVersionUID = 2532181609184500580L;
    private static final String BUNDLE = "resources.jmcim";

    @Autowired
    private transient TemplateService templateService;

    public boolean test(RequestContext ctx) {
        ctx.getMessageContext().addMessage(
                new MessageBuilder().error().source("jmcim_config").defaultText(Messages.get(BUNDLE,"jmcim_config.CantVerifyKey", LocaleContextHolder.getLocale())).build());
        return false;
    }

    public TemplateModel createTemplateModel(RequestContext ctx,String templateUUID) {
        if (templateUUID!=null && !templateUUID.trim().isEmpty()) {
            try {
                JCRSessionWrapper session = getCurrentUserSession(ctx);
                JCRNodeWrapper node = session.getNodeByUUID(templateUUID);
                return new TemplateModel(node);
            } catch (RepositoryException e) {
                logger.warn("Cant find template by uuid "+templateUUID, e);
            }
        }
        return new TemplateModel();
    }

    /* Template */
    public Boolean addActionTemplate(RequestContext ctx,TemplateModel model) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            templateService.createUpdateTemplate(session, getRenderContext(ctx), model);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_template");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            if (e instanceof MailChimpException && ((MailChimpException) e).message.contains("already exists"))
                model.setShowRestore(true);
            return false;
        }
    }

    public boolean removeTemplate(RequestContext ctx, String templateId) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper template = getNodeByUUID(templateId, session);
            templateService.deleteTemplate(template);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_template");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }
    public boolean updateTemplateContent(RequestContext ctx, String templateId) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper template = getNodeByUUID(templateId, session);
            templateService.updateTemplateContent(template,getRenderContext(ctx));
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_template");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    protected JCRNodeWrapper getNodeByUUID(String identifier, JCRSessionWrapper session) {
        try {
            return session.getNodeByUUID(identifier);
        } catch (RepositoryException e) {
            logger.error("Error retrieving node with UUID " + identifier, e);
        }
        return null;
    }

    protected JCRSessionWrapper getCurrentUserSession(RequestContext ctx) throws RepositoryException {
            RenderContext renderContext = getRenderContext(ctx);
            return renderContext.getMainResource().getNode().getSession();
    }

    protected RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext) ctx.getExternalContext().getRequestMap().get("renderContext");
    }

}
