/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings;

import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ecwid.mailchimp.MailChimpException;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.lists.MailChimpJahiaMergeVars;
import org.jahia.modules.mailchimp.api.lists.WebHookResult;
import org.jahia.modules.mailchimp.service.MailChimpService;
import org.jahia.modules.mailchimp.service.MailChimpSettings;
import org.jahia.modules.mailchimp.service.SubscriptionService;
import org.jahia.modules.mailchimp.sitesettings.model.WebHookModel;
import org.jahia.services.content.JCRContentUtils;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.jahia.services.render.URLGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.webflow.context.ExternalContext;
import org.springframework.webflow.core.collection.ParameterMap;
import org.springframework.webflow.execution.RequestContext;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

/**
 * Created by naf on 09.04.2015.
 */
public class ManageListsFlowHandler implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(ManageTemplateFlowHandler.class);
    private static final long serialVersionUID = 7394080046943107379L;

    @Autowired
    private transient MailChimpService mailChimpService;

    @Autowired
    private transient SubscriptionService subscriptionService;

    public boolean mapList(RequestContext ctx, String[] listIds) {
        try {

            subscriptionService.mapList(getRenderContext(ctx), listIds);

            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    public void debugContext(ExternalContext ectx, Object reqScope){
        if(null==ectx)
            return;
        ectx.toString();
    }

    public boolean removeList(RequestContext ctx, String listId) {
        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRSessionWrapper session = settings.getNode().getSession();
            JCRNodeWrapper listNode = settings.getListNode(listId);
            String url = listNode.getPropertyAsString("webhookUrl");
            if (null!=url && url.length()>0) {
                remWebHook(ctx, listNode.getName(), listNode.getPropertyAsString("webhookUrl"));
            }
            listNode.remove();
            session.save();
            return true;
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    public boolean addUpdateVarField(RequestContext ctx, String listId, String varFieldKey, String varFieldValue) {

        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRSessionWrapper session = settings.getNode().getSession();
            JCRNodeWrapper varFieldsNode = settings.getListNode(listId).getNode("j:fields");
            for (JCRNodeWrapper varFieldNode : varFieldsNode.getNodes()) {
                if (varFieldKey.equals(varFieldNode.getPropertyAsString("j:key"))) {
                    session.checkout(varFieldNode);
                    varFieldNode.setProperty("j:value", varFieldValue);
                    session.save();
                    return true;
                }
            }
            session.checkout(varFieldsNode);
            JCRNodeWrapper newField = varFieldsNode
                    .addNode(JCRContentUtils.findAvailableNodeName(varFieldsNode, "field"), "jmcim:varField");
            newField.setProperty("j:key", varFieldKey);
            newField.setProperty("j:value", varFieldValue);
            session.save();
            return true;
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    public boolean remVarField(RequestContext ctx, String listId, String varFieldKey) {
        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRSessionWrapper session = settings.getNode().getSession();
            if (null==varFieldKey) {
                return false;
            }
            JCRNodeWrapper varFieldsNode = settings.getListNode(listId).getNode("j:fields");
            for (JCRNodeWrapper varFieldNode : varFieldsNode.getNodes()) {
                if (varFieldKey.equals(varFieldNode.getPropertyAsString("j:key"))) {
                    session.checkout(varFieldsNode);
                    varFieldNode.remove();
                    session.save();
                    return true;
                }
            }
            return true;
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    public WebHookResult getWebHook(RequestContext ctx, String listId, String url) throws RepositoryException, MailChimpException, IOException {
        MailChimpSettings settings= getMailChimpSetting(ctx);
        return settings.getWebHook(listId,url);
    }

    public boolean remWebHook(RequestContext ctx, String listId, String webhookUrl) {
        MailChimpSettings settings = getMailChimpSetting(ctx);
        try {
            return settings.getAPI().webhookDel(listId, webhookUrl);
        } catch (Exception e) {
            if (e instanceof IOException || e instanceof MailChimpException) {
                MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
                ctx.getMessageContext().addMessage(
                        mb.defaultText(e.getMessage())
                                .build());
            }
            return false;
        }
    }

    public boolean editWebhook(RequestContext ctx, String listId) {
        MailChimpSettings settings = getMailChimpSetting(ctx);
        ParameterMap params = ctx.getRequestParameters();
        WebHookResult webhook = new WebHookResult(params.get("webhookurl"), params.asMap());
        try {
            MailChimpAPI api = settings.getAPI();
            if (null!=settings.getWebHook(listId, webhook.url)) {
                api.webhookDel(listId, webhook.url);
            }
            return api.webhookAdd(listId, webhook);
        } catch (Exception e) {
            if (e instanceof IOException || e instanceof MailChimpException) {
                MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
                ctx.getMessageContext().addMessage(
                        mb.defaultText(e.getMessage())
                                .build());
            } else {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean setWebhook(RequestContext rctx, String listId, boolean value) {
        RenderContext ctx = getRenderContext(rctx);
        MailChimpSettings settings = getMailChimpSetting(rctx);
        try {
            JCRNodeWrapper listNode = settings.getListNode(listId);
            MailChimpAPI api = settings.getAPI();
            String url = listNode.getPropertyAsString("webhookUrl");
            URLGenerator urlGenerator = ctx.getURLGenerator();

            String urlNew = urlGenerator.getServer()+
                    ((HttpServletResponse)rctx.getExternalContext().getNativeResponse())
                            .encodeURL(urlGenerator.getBaseLive()+listNode.getPath()+".jmcimWebhook.do");
            if (null==url || url.length()==0) {
                url = urlNew;
            }
            List<WebHookResult> webhooks = api.getWebHooks(listId);
            for (WebHookResult webHookResult : webhooks) {
                //remove old webhook from mailchimp
                if (url!=null && url.equals(webHookResult.url)) {
                    api.webhookDel(listId, url);
                    continue;
                }
                if (urlNew!=null && url.equals(webHookResult.url)) { // to be sure if sometime before same hook was inited with wron properties
                    api.webhookDel(listId, url);
                    continue;
                }
            }
            if (value) {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put("api", "true");
                param.put("admin", "true");
                param.put("user", "true");
                param.put("subscribe", "true");
                param.put("unsubscribe", "true");
                WebHookResult webhook = new WebHookResult(urlNew, param);
                api.webhookAdd(listId, webhook);
                listNode.setProperty("webhookUrl", urlNew);
            } else {
                listNode.setProperty("webhookUrl", (String)null);
            }
            listNode.getSession().save();
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            rctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
        }
        return false;
    }

    public boolean setWelcome(RequestContext ctx, String listId, String value) {
        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRNodeWrapper listNode = settings.getListNode(listId);
            listNode.setProperty(MailChimpSettings.LIST_WELCOME_OPTIN, value);
            listNode.getSession().save();
            return true;
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }


    public boolean setSubscribeForm(RequestContext ctx, String listId, String value) {
        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRSessionWrapper session = settings.getNode().getSession();
            JCRNodeWrapper listNode = settings.getListNode(listId);
            listNode.setProperty("subscribeForm", value);
            session.save();
            return true;
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return false;
        }
    }

    public String getSubscribeForm(RequestContext ctx, String listId) {
        try {
            MailChimpSettings settings = getMailChimpSetting(ctx);
            JCRNodeWrapper listNode = settings.getListNode(listId);
            return listNode.getPropertyAsString("subscribeForm");
        } catch (RepositoryException e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_lists");
            ctx.getMessageContext().addMessage(
                    mb.defaultText(e.getMessage())
                            .build());
            return null;
        }
    }
    public List<WebHookModel> getWebHooksModel(RequestContext ctx, String listId) throws RepositoryException, MailChimpException, IOException {
        MailChimpSettings settings= getMailChimpSetting(ctx);
        return settings.getWebHooksModel(listId);
    }
    public MailChimpJahiaMergeVars getMergeVarsSync(RequestContext ctx, String listId) throws RepositoryException, MailChimpException, IOException {
        MailChimpSettings settings= getMailChimpSetting(ctx);
        return settings.getMergeVarsSync(listId);
    }
    public MailChimpSettings getMailChimpSetting(RequestContext ctx) {
        RenderContext renderContext = getRenderContext(ctx);
        return mailChimpService.getSettings(renderContext.getMainResource().getNode());
    }

    private RenderContext getRenderContext(RequestContext ctx) {
        return (RenderContext)ctx.getExternalContext().getRequestMap().get("renderContext");
    }
}
