/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings;

import org.jahia.modules.mailchimp.service.CampaignService;
import org.jahia.modules.mailchimp.service.MailChimpService;
import org.jahia.modules.mailchimp.service.MailChimpSettings;
import org.jahia.modules.mailchimp.sitesettings.model.CampaignDetails;
import org.jahia.modules.mailchimp.sitesettings.model.CampaignSetModel;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.webflow.execution.RequestContext;

import javax.jcr.RepositoryException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Alexander Storozhuk on 06.05.2015.
 * Time: 15:04
 */
public class ManageCampaignsFlowHandler extends ManageTemplateFlowHandler implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(ManageCampaignsFlowHandler.class);
    private static final long serialVersionUID = 3415267309184510580L;

    @Autowired
    private transient CampaignService campaignService;

    @Autowired
    private transient MailChimpService mailChimpService;


    public CampaignSetModel createCampaignSetModel(RequestContext ctx, String campaignSetUUID) {
        if (campaignSetUUID != null && !campaignSetUUID.trim().isEmpty()) {
            try {
                JCRSessionWrapper session = getCurrentUserSession(ctx);
                JCRNodeWrapper node = session.getNodeByUUID(campaignSetUUID);
                return new CampaignSetModel(node);
            } catch (RepositoryException e) {
                logger.warn("Cant find campaignSet by uuid " + campaignSetUUID, e);
            }
        }
        return new CampaignSetModel();
    }


    /* Campaign Set Add */
    public Boolean addActionCampaignSet(RequestContext ctx, CampaignSetModel campModel) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            campaignService.createUpdateCampaignSet(session, getRenderContext(ctx), campModel);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaigns");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    /* Campaign Set Delete */
    public boolean removeCampaignSet(RequestContext ctx, String cmSetId) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campaignSetNode = getNodeByUUID(cmSetId, session);
            campaignService.deleteCampaignSet(campaignSetNode);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaignSet");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    /* Campaign Set Update */
    public boolean updateCampaignSetContent(RequestContext ctx, String cmSetId) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campaignSetNode = getNodeByUUID(cmSetId, session);
            campaignService.updateCampaignSet(campaignSetNode, getRenderContext(ctx));
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaignSet");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    /* Campaign Delete */
    public  boolean removeCampaign(RequestContext ctx, String cmId){
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campaignNode = getNodeByUUID(cmId, session);
            campaignService.deleteCampaign(campaignNode);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaign");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    public  boolean createCampaign(RequestContext ctx, String cmId){
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campSetNode = getNodeByUUID(cmId, session);
            RenderContext renderContext = getRenderContext(ctx);
            campaignService.createCampaign(campSetNode, renderContext);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaign");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    /* Campaign manual send */
    public  boolean sendCampaign(RequestContext ctx, String cmId){
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campaignNode = getNodeByUUID(cmId, session);
            campaignService.sendCampaign(campaignNode);
            return true;
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaign");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            return false;
        }
    }

    /* Campaign set details */
    public Collection<CampaignDetails> campaignsDetails(RequestContext ctx, String cmSetId) {
        try {
            JCRSessionWrapper session = getCurrentUserSession(ctx);
            JCRNodeWrapper campaignSetNode = session.getNodeByUUID(cmSetId);
            return campaignService.getCampaignsDetails(campaignSetNode);
        } catch (Exception e) {
            MessageBuilder mb = new MessageBuilder().error().source("jmcim_campaignSet");
            ctx.getMessageContext().addMessage(mb.defaultText(e.getMessage()).build());
            logger.warn("Can't get campaigns details for campaignSet by uuid " + cmSetId, e);
        }
        return new ArrayList<CampaignDetails>();
    }


    public String getListRootPath(RequestContext ctx) throws RepositoryException{
        String path = getMailChimpSetting(ctx).getListsNode().getPath();
       return path;
    }
    public MailChimpSettings getMailChimpSetting(RequestContext ctx) {
        RenderContext renderContext = getRenderContext(ctx);
        return mailChimpService.getSettings(renderContext.getMainResource().getNode());
    }

}
