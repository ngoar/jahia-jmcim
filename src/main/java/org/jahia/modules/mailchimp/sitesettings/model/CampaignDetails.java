/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings.model;

import com.ecwid.mailchimp.MailChimpObject;
import org.jahia.modules.mailchimp.api.campaigns.list.Data;
import org.jahia.services.content.JCRNodeWrapper;

import java.util.AbstractMap;

/**
 * Created by Alexander Storozhuk on 15.05.2015.
 * Time: 13:10
 */
public class CampaignDetails {

    private JCRNodeWrapper node;
    private Data dataInfo;
    private Summary summary;

    public CampaignDetails(JCRNodeWrapper node) {
        this.node = node;
    }

    public Data getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(Data dataInfo) {
        this.dataInfo = dataInfo;
        Object tmpSummary = dataInfo.get("summary");
        if (null != tmpSummary && tmpSummary instanceof MailChimpObject) {
            this.summary = new Summary((AbstractMap) tmpSummary);
        }
    }

    public JCRNodeWrapper getNode() {
        return node;
    }

    public void setNode(JCRNodeWrapper node) {
        this.node = node;
    }

    public Summary getSummary() {
        return summary;
    }

    public static class Summary {
        private String syntaxErrors;
        private String hardBounces;
        private String softBounces;
        private String unsubscribes;
        private String emailsSent;

        public Summary(AbstractMap params) {
            this.syntaxErrors = String.format("%.0f",(Double)params.get("syntax_errors"));
            this.hardBounces = String.format("%.0f", (Double) params.get("hard_bounces"));
            this.softBounces = String.format("%.0f", (Double) params.get("soft_bounces"));
            this.unsubscribes = String.format("%.0f", (Double) params.get("unsubscribes"));
            this.emailsSent = String.format("%.0f", (Double) params.get("emails_sent"));
        }

        public String getSyntaxErrors() {
            return syntaxErrors;
        }

        public String getHardBounces() {
            return hardBounces;
        }

        public String getSoftBounces() {
            return softBounces;
        }

        public String getUnsubscribes() {
            return unsubscribes;
        }
        public String getEmailsSent() {
            return emailsSent;
        }
    }
}
