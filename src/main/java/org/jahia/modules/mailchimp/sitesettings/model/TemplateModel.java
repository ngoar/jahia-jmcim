/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.sitesettings.model;

import org.jahia.services.content.JCRNodeWrapper;

import javax.jcr.RepositoryException;
import java.io.Serializable;

/**
 * Created by: Boris
 * Date: 4/24/2015
 * Time: 7:02 AM
 */
public class TemplateModel implements Serializable {
    String templateUUID;
    String title;
    String contentNode;
    String contentNodeDecoy;
    boolean isUpdate = false;
    boolean restore = false;
    boolean showRestore = false;

    public TemplateModel(JCRNodeWrapper node) throws RepositoryException {
        setTemplateUUID(node.getIdentifier());
        setTitle(node.getPropertyAsString("jcr:title"));
        setUpdate(true);
        JCRNodeWrapper contentNode = (JCRNodeWrapper) node.getProperty("contentNode").getNode();
        setContentNode(contentNode.getIdentifier());
        setContentNodeDecoy(contentNode.getDisplayableName());
    }

    public TemplateModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentNode() {
        return contentNode;
    }

    public void setContentNode(String contentNode) {
        this.contentNode = contentNode;
    }

    public String getContentNodeDecoy() {
        return contentNodeDecoy;
    }

    public void setContentNodeDecoy(String contentNodeDecoy) {
        this.contentNodeDecoy = contentNodeDecoy;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getTemplateUUID() {
        return templateUUID;
    }

    public void setTemplateUUID(String templateUUID) {
        this.templateUUID = templateUUID;
    }

    public boolean isRestore() {
        return restore;
    }

    public void setRestore(boolean restore) {
        this.restore = restore;
    }

    public boolean isShowRestore() {
        return showRestore;
    }

    public void setShowRestore(boolean showRestore) {
        this.showRestore = showRestore;
    }
}
