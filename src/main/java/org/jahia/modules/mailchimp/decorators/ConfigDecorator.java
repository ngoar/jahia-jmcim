/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.decorators;

import javax.jcr.*;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

import org.jahia.modules.mailchimp.service.MailChimpSettings;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRPropertyWrapper;
import org.jahia.services.content.LazyPropertyIterator;
import org.jahia.services.content.decorator.JCRNodeDecorator;

/**
 * Hide list of properties to make them more secure
 * Created by: Boris
 * Date: 5/11/2015
 * Time: 5:49 PM
 */
public class ConfigDecorator extends JCRNodeDecorator {

    private Set<String> securePropertyNames = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
            MailChimpSettings.API_KEY
    )));

    public Set<String> getSecurePropertyNames() {
        return securePropertyNames;
    }

    public ConfigDecorator(JCRNodeWrapper node) {
        super(node);
    }

    public JCRPropertyWrapper getSecureProperty(String s) throws RepositoryException {
        return super.getProperty(s);
    }

    public String getSecurePropertyAsString(String s) {
        return super.getPropertyAsString(s);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Value value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, value);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Value value, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, value, i);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Value[] values) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, values);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Value[] values, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, values, i);
    }


    public JCRPropertyWrapper setSecureProperty(String s, String[] strings) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, strings);
    }


    public JCRPropertyWrapper setSecureProperty(String s, String[] strings, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, strings, i);
    }


    public JCRPropertyWrapper setSecureProperty(String s, String s1) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, s1);
    }


    public JCRPropertyWrapper setSecureProperty(String s, String s1, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, s1, i);
    }


    public JCRPropertyWrapper setSecureProperty(String s, InputStream inputStream) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, inputStream);
    }


    public JCRPropertyWrapper setSecureProperty(String name, Binary value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(name, value);
    }


    public JCRPropertyWrapper setSecureProperty(String s, boolean b) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, b);
    }


    public JCRPropertyWrapper setSecureProperty(String s, double v) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, v);
    }


    public JCRPropertyWrapper setSecureProperty(String name, BigDecimal value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(name, value);
    }


    public JCRPropertyWrapper setSecureProperty(String s, long l) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, l);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Calendar calendar) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, calendar);
    }


    public JCRPropertyWrapper setSecureProperty(String s, Node node) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        return super.setProperty(s, node);
    }


// Override property methods to hide secure property
    @Override
    public PropertyIterator getProperties() throws RepositoryException {
        final Locale locale = getSession().getLocale();
        return new LazyPropertyIterator(this, locale) {
            @Override
            protected PropertyIterator getPropertiesIterator() {
                if (propertyIterator==null) {
                    propertyIterator = new FilteredPropertyIterator(super.getPropertiesIterator(), securePropertyNames);
                }
                return propertyIterator;
            }

            @Override
            protected PropertyIterator getI18NPropertyIterator() {
                if (i18nPropertyIterator==null) {
                    return new FilteredPropertyIterator(super.getI18NPropertyIterator(), securePropertyNames);
                }
                return i18nPropertyIterator;
            }
        };
    }

    @Override
    public JCRPropertyWrapper getProperty(String s) throws PathNotFoundException, RepositoryException {
        if (!canGetProperty(s)) {
            throw new PathNotFoundException(s);
        }
        return super.getProperty(s);
    }

    private boolean canGetProperty(String name) {
        return !securePropertyNames.contains(name);
    }

    @Override
    public boolean hasProperty(String s) throws RepositoryException {
        return canGetProperty(s) && super.hasProperty(s);
    }


    @Override
    public Map<String, String> getPropertiesAsString() throws RepositoryException {
        Map<String, String> entries = super.getPropertiesAsString();
        Map<String, String> map = new HashMap<String, String>();

        for (Map.Entry<String, String> entry : entries.entrySet()) {
            if (canGetProperty(entry.getKey())) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map;
    }

    @Override
    public String getPropertyAsString(String name) {
        if (!canGetProperty(name)) {
            return null;
        }
        return super.getPropertyAsString(name);
    }


    @Override
    public JCRPropertyWrapper setProperty(String s, Value value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, value);
    }

    private void checkWrite(String s) throws AccessDeniedException {
        if (!canGetProperty(s)) {
            throw new AccessDeniedException("Cannot update secure property");
        }
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, Value value, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, value, i);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, Value[] values) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, values);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, Value[] values, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, values, i);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, String[] strings) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, strings);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, String[] strings, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, strings, i);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, String s1) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, s1);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, String s1, int i) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, s1, i);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, InputStream inputStream) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, inputStream);
    }

    @Override
    public JCRPropertyWrapper setProperty(String name, Binary value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(name);
        return super.setProperty(name, value);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, boolean b) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, b);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, double v) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, v);
    }

    @Override
    public JCRPropertyWrapper setProperty(String name, BigDecimal value) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(name);
        return super.setProperty(name, value);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, long l) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, l);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, Calendar calendar) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, calendar);
    }

    @Override
    public JCRPropertyWrapper setProperty(String s, Node node) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
        checkWrite(s);
        return super.setProperty(s, node);
    }


}