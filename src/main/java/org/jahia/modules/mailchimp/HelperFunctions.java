/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp;

import javax.jcr.RepositoryException;

import org.jahia.modules.mailchimp.service.MailChimpService;
import org.jahia.modules.mailchimp.service.MailChimpSettings;
import org.jahia.modules.mailchimp.service.SubscriptionService;
import org.jahia.modules.mailchimp.service.exceptions.MailChimpNotConfiguredException;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.usermanager.JahiaUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by naf on 06.04.2015.
 */
public class HelperFunctions {
    private static Logger logger = LoggerFactory.getLogger(HelperFunctions.class);

    public static MailChimpSettings settings(JCRNodeWrapper target) throws MailChimpNotConfiguredException {
        MailChimpService service = (MailChimpService) SpringContextSingleton.getBean(MailChimpService.BEAN_NAME);
        return service.getSettings(target);

    }

    public static boolean hasSubscribed(JCRNodeWrapper target, JahiaUser user) {
        SubscriptionService service = (SubscriptionService) SpringContextSingleton.getBean(SubscriptionService.BEAN_NAME);
        try {
            if(null==target||null==user) return false;
            String identifier = target.isNodeType(SubscriptionService.JMCIM_LIST)?target.getIdentifier():target.getPropertyAsString(SubscriptionService.J_TARGET);
            return service.getSubscription(identifier, user, JCRSessionFactory.getInstance().getCurrentUserSession("live")) != null;
        } catch (RepositoryException e) {
            logger.error("Error testing if user: " + user.getUserKey() + " has subscribed to node: " + target.getDisplayableName());
        }
        return false;
    }
}
