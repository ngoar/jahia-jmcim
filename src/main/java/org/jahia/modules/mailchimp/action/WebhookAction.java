/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.action;

import org.jahia.bin.Action;
import org.jahia.bin.ActionResult;
import org.jahia.modules.mailchimp.service.SubscriptionService;
import org.jahia.modules.mailchimp.service.exceptions.MailChimpSubscriptionException;
import org.jahia.services.content.JCRCallback;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.JCRTemplate;
import org.jahia.services.render.RenderContext;
import org.jahia.services.render.Resource;
import org.jahia.services.render.URLResolver;
import org.jahia.services.usermanager.JahiaUser;
import org.jahia.utils.i18n.Messages;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

/**
 * Created by naf on 12.05.2015.
 */
public class WebhookAction  extends Action {
    private static final String BUNDLE = "resources.jmcim";
    private static final Logger logger = LoggerFactory.getLogger(SubscribeAction.class);

    @Autowired
    private SubscriptionService subscriptionService;


    @Override
    public ActionResult doExecute(final HttpServletRequest req, final RenderContext renderContext,
                                  final Resource resource, JCRSessionWrapper session, final Map<String, List<String>> parameters, URLResolver urlResolver) throws Exception {
        return JCRTemplate.getInstance().doExecuteWithSystemSession(null, "live", new JCRCallback<ActionResult>() {
            public ActionResult doInJCR(JCRSessionWrapper session) throws RepositoryException {

                try {
                    final JCRNodeWrapper node =  session.getNodeByIdentifier(resource.getNode().getIdentifier());
                    if (!node.isNodeType(SubscriptionService.JMCIM_LIST))
                        return null;
                    String type = getParameter(parameters, "type");
                    String subscribeId = getParameter(parameters, "data[id]");
                    if(null==type)
                        return new ActionResult(SC_OK, null, new JSONObject()
                                .put("status", "ok")
                                .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.unsubscribed", renderContext.getMainResourceLocale())));

                    if ("unsubscribe".equals(type)) {
                        try {
                            node.getAbsoluteUrl(req);
                            subscriptionService.unsubscribe(session,node, subscribeId);
                            return new ActionResult(SC_OK, null, new JSONObject()
                                    .put("status", "ok")
                                    .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.unsubscribed", renderContext.getMainResourceLocale())));
                        } catch (MailChimpSubscriptionException e) {
                            logger.error("Error", e);
                            return new ActionResult(SC_OK, null, new JSONObject()
                                    .put("status", "err")
                                    .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.error_unsubscribe", renderContext.getMainResourceLocale())));
                        }
                    }
                    if ("subscribe".equals(type)) {
                        try {
                            if (subscriptionService.getSubscription(node, subscribeId) != null) {
                                return new ActionResult(SC_OK, null, new JSONObject()
                                        .put("status", "ok")
                                        .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.already_subscribed", renderContext.getMainResourceLocale())));
                            }

                            JCRNodeWrapper subs=subscriptionService.createSubscription(session,node, null, getParameter(parameters, "data[email]"), subscribeId, getParameter(parameters, "data[web_id]"));
                            if(null!=subs)
                                return new ActionResult(SC_OK, null, new JSONObject()
                                        .put("status", "ok")
                                        .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.subscribed", renderContext.getMainResourceLocale())));
                            else
                                return new ActionResult(SC_OK, null, new JSONObject()
                                        .put("status", "err")
                                        .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg.error_subscribe", renderContext.getMainResourceLocale())));
                        } catch (MailChimpSubscriptionException e) {
                            return new ActionResult(SC_OK, null, new JSONObject()
                                    .put("status", "error")
                                    .put("code", e.getStatus().name())
                                    .put("msg", Messages.get(BUNDLE, "jmcim.subscriptions.msg." + e.getStatus(),
                                            renderContext.getMainResourceLocale(), e.getStatus().name())));
                        }
                    }
                } catch (JSONException e) {
                    logger.error("Error", e);
                    return new ActionResult(SC_INTERNAL_SERVER_ERROR, null, null);
                }
                return null;
            }
        });

    }
}
