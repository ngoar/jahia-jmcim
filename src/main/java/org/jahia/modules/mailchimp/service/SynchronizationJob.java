/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import com.ecwid.mailchimp.MailChimpException;
import org.jahia.bin.ActionResult;
import org.jahia.services.content.JCRCallback;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.JCRTemplate;
import org.jahia.services.scheduler.BackgroundJob;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.RepositoryException;
import java.io.IOException;

/**
 * Created by: Boris
 * Date: 4/15/2015
 * Time: 12:50 PM
 */
public class SynchronizationJob extends BackgroundJob {

    @Autowired
    private SubscriptionService jmcimSubscriptionService;

    @Override
    public void executeJahiaJob(JobExecutionContext ctx) throws Exception {
        if(!ctx.getJobDetail().getJobDataMap().containsKey("jmcimSubscriptionService"))
            return;
        jmcimSubscriptionService=(SubscriptionService)ctx.getJobDetail().getJobDataMap().get("jmcimSubscriptionService");
        //syncSubscribers
        JCRTemplate.getInstance().doExecuteWithSystemSession(null, "default", new JCRCallback<ActionResult>() {

            @Override
            public ActionResult doInJCR(JCRSessionWrapper session) throws RepositoryException {
                JCRNodeWrapper sites = session.getRootNode().getNode("sites");
                for (JCRNodeWrapper site : sites.getNodes()) {
                    if(site.isNodeType("jnt:virtualsite")&&!"systemsite".equals(site.getPropertyAsString("j:nodename"))){
                        try {
                            JCRNodeWrapper settings=site.getNode(SubscriptionService.JMCIM_CONFIG);
                            jmcimSubscriptionService.syncLists(session,settings);

                        } catch (RepositoryException e) {
                        }
                    }
                }


                return null;
            }
        });
        JCRTemplate.getInstance().doExecuteWithSystemSession(null, "live", new JCRCallback<ActionResult>() {

            @Override
            public ActionResult doInJCR(JCRSessionWrapper session) throws RepositoryException {
                JCRNodeWrapper sites = session.getRootNode().getNode("sites");
                for (JCRNodeWrapper site : sites.getNodes()) {
                    if(site.isNodeType("jnt:virtualsite")&&!"systemsite".equals(site.getPropertyAsString("j:nodename"))){
                        try {
                            jmcimSubscriptionService.syncSubscribers(site);
                        } catch (RepositoryException e) {
                        } catch (MailChimpException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


                return null;
            }
        });

    }

    public void setJmcimSubscriptionService(SubscriptionService jmcimSubscriptionService) {
        this.jmcimSubscriptionService = jmcimSubscriptionService;
    }

    public SubscriptionService getJmcimSubscriptionService() {
        return jmcimSubscriptionService;
    }
}
