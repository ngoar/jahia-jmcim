/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import com.ecwid.mailchimp.MailChimpException;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.helpers.AccountDetailsResult;
import org.jahia.modules.mailchimp.api.ping.PingResult;
import org.jahia.modules.mailchimp.service.exceptions.MailChimpNotConfiguredException;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.decorator.JCRSiteNode;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.Calendar;

/**
 * Main MailChimp service
 */
public class MailChimpService {
    public static final String BEAN_NAME = "jmcimMailChimpService";

//    private static final Logger logger = LoggerFactory.getLogger(MailChimpService.class);
    public static final String settingsNodeName = "jmcimConfig";


    /**
     * Find MailChimp setting node
     *
     * @param node SiteNode or any other node on site
     * @return Settings node
     * @throws MailChimpNotConfiguredException
     */
    public JCRNodeWrapper getSettingsNode(JCRNodeWrapper node) throws MailChimpNotConfiguredException {
        try {
            JCRSiteNode resolveSite = node.getResolveSite();
            return resolveSite.getNode(settingsNodeName);
        } catch (RepositoryException e) {
            throw new MailChimpNotConfiguredException(e);
        }
    }

    /**
     * Create  MailChimp settings object
     *
     * @param node SiteNode or any other node on site
     * @return
     */
    public MailChimpSettings getSettings(JCRNodeWrapper node) {
        MailChimpSettings mailChimpSettings = new MailChimpSettings(this);
        try {
            JCRNodeWrapper settingsNode = getSettingsNode(node);
            return new MailChimpSettings(settingsNode, this);
        } catch (MailChimpNotConfiguredException e) {
            return mailChimpSettings;
        }
    }

    /**
     * Create API object
     *
     * @param apiKey API-key for calls
     * @return
     */
    public MailChimpAPI createAPI(String apiKey) {
        return new MailChimpAPI(apiKey);
    }


    /**
     * Validate and store
     *
     * @param node      SiteNode or any other node on site
     * @param newApiKey API-key or null if you need revalidate stored one
     * @return
     */
    public PingResult validateKey(JCRNodeWrapper node, final String newApiKey) throws RepositoryException, IOException, MailChimpException {
        final MailChimpSettings settings = getSettings(node);
        final String key = newApiKey != null ? newApiKey : settings.getApiKey();
        MailChimpAPI api = createAPI(key);
        final PingResult validate = api.validate(key);
        JCRNodeWrapper settingsNode = settings.getNodeInSession(node.getSession());
        settingsNode.getNode(MailChimpSettings.SECURE_NODE_NAME).setProperty(MailChimpSettings.API_KEY, key);
        settingsNode.setProperty(MailChimpSettings.API_KEY_VALID, validate.isOk());
        settingsNode.setProperty(MailChimpSettings.API_KEY_VALID_DATE, Calendar.getInstance());
        if (validate.isOk()) {
            AccountDetailsResult accountDetailsResult = api.getAccountDetails();
            final String accountUUID = accountDetailsResult.user_id;
            if ((!key.equals(settings.getApiKey()) && null != accountUUID)) {
                settingsNode.setProperty(MailChimpSettings.API_KEY_VALID_DATE, Calendar.getInstance());
                settingsNode.setProperty(MailChimpSettings.ACCOUNT_UUID, accountUUID);
            }
        }
        node.getSession().save();
        return validate;
    }


}