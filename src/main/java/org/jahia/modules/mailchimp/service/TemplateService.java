/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import com.ecwid.mailchimp.MailChimpException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.StatusLine;
import org.apache.commons.httpclient.methods.GetMethod;
import org.jahia.bin.Render;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.templates.TemplateListResult;
import org.jahia.modules.mailchimp.sitesettings.model.TemplateModel;
import org.jahia.params.valves.TokenAuthValveImpl;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.notification.HtmlExternalizationService;
import org.jahia.services.render.RenderContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import static org.apache.commons.httpclient.HttpStatus.SC_OK;


/**
 * MailChimp template service.
 * Created by Alexander Storozhuk on 08.04.2015.
 * Time: 18:02
 */
public class TemplateService {
    private static final Logger logger = LoggerFactory.getLogger(TemplateService.class);

    @Autowired
    private MailChimpService mailChimpService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private HtmlExternalizationService htmlExternalizationService;
    private static final String templatesNode = "templates";
    public static final String BEAN_NAME = "jmcimTemplateService";
    private String localServerURL;

    /**
     * Render mail template content
     *
     * @param renderContext
     * @param renderNode
     * @return
     */
    public String renderNodeExternally(RenderContext renderContext, JCRNodeWrapper renderNode) {
        String requestURL = "";
        String out = null;
        HttpClient httpClient = new HttpClient();
        GetMethod getMethod = null;
        try {
            requestURL = localServerURL+Render.getRenderServletPath()+"/live/"
                    +renderNode.getResolveSite().getDefaultLanguage()+renderNode.getPath()+".html";
            getMethod = new GetMethod(requestURL);
            getMethod.setRequestHeader("jahiatoken", TokenAuthValveImpl.addToken(renderNode.getSession().getUser()));

            logger.info("Retrieving resource at absolute URL "+requestURL+"...");
            httpClient.executeMethod(getMethod);
            StatusLine statusLine = getMethod.getStatusLine();

            if (statusLine!=null && statusLine.getStatusCode()==SC_OK) {
                out = getMethod.getResponseBodyAsString();
                out = htmlExternalizationService.externalize(out, renderContext);
            } else {
                logger.warn("Connection to URL: "+requestURL+" failed with status "+statusLine);
                out = null;
            }
            if (out==null) {
                logger.error("Couldn't resolve a resource stream, aborting node export for URL "+requestURL);
                if (getMethod!=null) {
                    getMethod.releaseConnection();
                }
                return null;
            }
        } catch (IOException e) {
            logger.error("Error while downloading URL "+requestURL, e);

        } catch (RepositoryException rex) {
            logger.error("Error while downloading URL "+requestURL, rex);
        } finally {
            if (getMethod!=null) {
                getMethod.releaseConnection();
            }
        }
        return out;
    }

    /**
     * Delete local template mapping and template in MailChimp
     *
     * @param templateNode
     * @throws RepositoryException
     * @throws MailChimpException
     * @throws IOException
     */
    public void deleteTemplate(JCRNodeWrapper templateNode) throws RepositoryException, MailChimpException, IOException {
        JCRSessionWrapper session = templateNode.getSession();
        MailChimpSettings mailChimp = mailChimpService.getSettings(templateNode);
        Integer mailchimpId = (int)templateNode.getProperty("mailchimpId").getLong();
        try {
            mailChimp.getAPI().deleteTemplate(mailchimpId);
        } catch (MailChimpException mex) {
            if (mex.code==506 || mex.code==504) {
                logger.warn("MailChimp template "+templateNode.getDisplayableName()+"was already deleted in MailChimp ");
            } else {
                throw mex;
            }
        }
        JCRNodeWrapper contentNode = (JCRNodeWrapper)templateNode.getProperty("contentNode").getNode();
        templateNode.remove();
        session.save();
        if (fixContentNodeReference(contentNode)) {
            session.save();
        }
    }


    /**
     * Remove hasTemplate mixing if need
     *
     * @param contentNode
     * @return tru if node was changed
     * @throws RepositoryException
     */
    private boolean fixContentNodeReference(JCRNodeWrapper contentNode) throws RepositoryException {
        if (!contentNode.getWeakReferences("contentNode").hasNext()) {
            contentNode.removeMixin("jmcmix:hasTemplate");
            return true;
        }

        return false;
    }

    /**
     * Update templeate content in MailChimp
     *
     * @param templateNode
     * @param renderContext
     * @throws RepositoryException
     * @throws IOException
     * @throws MailChimpException
     */
    public void updateTemplateContent(JCRNodeWrapper templateNode, RenderContext renderContext) throws RepositoryException, IOException, MailChimpException {
        MailChimpSettings settings = mailChimpService.getSettings(templateNode);
        JCRNodeWrapper contentNode = (JCRNodeWrapper)templateNode.getProperty("contentNode").getNode();
        int id = (int)templateNode.getProperty("mailchimpId").getLong();
        String html = renderNodeExternally(renderContext, contentNode);
        settings.getAPI().updateTemplate(id, null, html);
    }

    /**
     * Create (unDelete) or update template mapping
     *
     * @param session
     * @param renderContext
     * @param model
     * @throws RepositoryException
     * @throws IOException
     * @throws MailChimpException
     */
    public void createUpdateTemplate(JCRSessionWrapper session, RenderContext renderContext, TemplateModel model) throws RepositoryException, IOException, MailChimpException {
        MailChimpSettings settings = mailChimpService.getSettings(renderContext.getSite());
        JCRNodeWrapper templatesLists = settings.getTemplatesNode();

        JCRNodeWrapper contentNode = session.getNodeByUUID(model.getContentNode());
        String pageHtml = renderNodeExternally(renderContext, contentNode);

        MailChimpAPI api = settings.getAPI();
        JCRNodeWrapper newTemplate;
        JCRNodeWrapper oldContentNode = null;
        String title = model.getTitle();
        if (model.isUpdate()) {
            newTemplate = session.getNodeByUUID(model.getTemplateUUID());
            api.updateTemplate((int)newTemplate.getProperty("mailchimpId").getLong(), title, pageHtml);
            if (newTemplate.getProperty("contentNode").getString().equals(model.getContentNode())) {
                oldContentNode = (JCRNodeWrapper)newTemplate.getProperty("contentNode").getNode(); //getContextualizedNode();
            }
            session.checkout(newTemplate);
        } else {
            Integer existsTemplId = null;
            if (!model.isRestore())
                existsTemplId = api.addTemplate(title, pageHtml);
            else {
                List<TemplateListResult.User> listTemplate = api.getTemplatesList();
                for (TemplateListResult.User templ : listTemplate) {
                    if (model.getTitle().equals(templ.name) && !templ.active) {
                        existsTemplId = templ.id;
                        break;
                    }
                }
                if (null != existsTemplId) {
                    api.unDeleteTemplate(existsTemplId);
                    api.updateTemplate(existsTemplId, title, pageHtml);
                } else {
                    existsTemplId = api.addTemplate(title, pageHtml);
                }
            }
            newTemplate = templatesLists.addNode(String.valueOf(existsTemplId), "jmcim:Template");
            newTemplate.setProperty("mailchimpId", existsTemplId);
            newTemplate.setProperty("scheduled", Calendar.getInstance());
            contentNode.addMixin("jmcmix:hasTemplate");
        }
        newTemplate.setProperty("jcr:title", title);
        newTemplate.setProperty("contentNode", contentNode);
        newTemplate.setProperty("needUpdate", false);
        session.save();
        if (oldContentNode!=null && fixContentNodeReference(contentNode)) {
            session.save();
        }
    }


    public HtmlExternalizationService getHtmlExternalizationService() {
        return htmlExternalizationService;
    }

    public void setHtmlExternalizationService(HtmlExternalizationService htmlExternalizationService) {
        this.htmlExternalizationService = htmlExternalizationService;
    }

    public void setLocalServerURL(String localServerURL) {
        this.localServerURL = localServerURL;
    }

    public String getLocalServerURL() {
        return localServerURL;
    }

    public MailChimpService getMailChimpService() {
        return mailChimpService;
    }


}