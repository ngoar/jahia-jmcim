/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import org.jahia.bin.ActionResult;
import org.jahia.bin.Render;
import org.jahia.modules.mailchimp.action.CreateCampaignAction;
import org.jahia.params.valves.TokenAuthValveImpl;
import org.jahia.services.SpringContextSingleton;
import org.jahia.services.content.JCRCallback;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.JCRTemplate;
import org.jahia.services.notification.HttpClientService;
import org.jahia.services.scheduler.BackgroundJob;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.json.JSONObject;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_OK;

/**
 * Created by Alexander Storozhuk on 13.05.2015.
 * Time: 19:36
 */
public class CampaignSendJob extends BackgroundJob{

    private static final Logger logger = LoggerFactory.getLogger(CampaignSendJob.class);


    @Autowired
    private transient HttpClientService httpClientService;
    @Autowired
    private transient TemplateService templateService;
    @Autowired
    private JahiaUserManagerService userManagerService;


    @Override
    public void executeJahiaJob(JobExecutionContext ctx) throws Exception {
        final String cmSetId = (String) ctx.getJobDetail().getJobDataMap().get(CampaignService.CAMPAIGN_SET_KEY_ID);
        if (null == cmSetId)
            return;
        if (null == templateService || null == httpClientService|| null == userManagerService) {
            setTemplateService((TemplateService) SpringContextSingleton.getBean(TemplateService.BEAN_NAME));
            setHttpClientService((HttpClientService) SpringContextSingleton.getBean(HttpClientService.class.getSimpleName()));
            setUserManagerService((JahiaUserManagerService) SpringContextSingleton.getBean(JahiaUserManagerService.class.getSimpleName()));
        }

        JCRTemplate.getInstance().doExecuteWithSystemSession(null, "default", new JCRCallback<ActionResult>() {
            @Override
            public ActionResult doInJCR(JCRSessionWrapper session) throws RepositoryException {
                JCRNodeWrapper campSetNode = session.getNodeByUUID(cmSetId);

                Map<String, String> headers = new HashMap<String, String>();
                headers.put("jahiatoken", TokenAuthValveImpl.addToken(userManagerService.lookupUser("root").getJahiaUser()));
                headers.put("accept", "text/plain");
                String out = httpClientService.executePost(
                        templateService.getLocalServerURL() + Render.getRenderServletPath() + "/default/"
                                + campSetNode.getResolveSite().getDefaultLanguage() + campSetNode.getPath() +
                                "."+ CreateCampaignAction.BEAN_NAME+".do", null, headers);
                logger.info(out);
                return new ActionResult(SC_OK, null, new JSONObject());
            }
        });

    }

    public HttpClientService getHttpClientService() {
        return httpClientService;
    }

    public void setHttpClientService(HttpClientService httpClientService) {
        this.httpClientService = httpClientService;
    }

    public TemplateService getTemplateService() {
        return templateService;
    }

    public void setTemplateService(TemplateService templateService) {
        this.templateService = templateService;
    }

    public JahiaUserManagerService getUserManagerService() {
        return userManagerService;
    }

    public void setUserManagerService(JahiaUserManagerService userManagerService) {
        this.userManagerService = userManagerService;
    }
}
