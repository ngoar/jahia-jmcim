/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.servlet.ServletRequest;
import java.awt.image.renderable.RenderContext;
import java.io.IOException;
import java.util.*;

import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.method.v2_0.lists.Email;
import com.ecwid.mailchimp.method.v2_0.lists.ListMethodResult;
import com.ecwid.mailchimp.method.v2_0.lists.MemberInfoData;
import com.ecwid.mailchimp.method.v2_0.lists.MembersResult;
import org.apache.commons.lang.StringUtils;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.lists.MailChimpJahiaMergeVars;
import org.jahia.modules.mailchimp.api.lists.WebHookResult;
import org.jahia.modules.mailchimp.service.exceptions.MailChimpSubscriptionException;
import org.jahia.modules.mailchimp.service.model.MailChimpJahiaList;
import org.jahia.modules.mailchimp.service.model.Subscription;
import org.jahia.services.content.*;
import org.jahia.services.mail.MailService;
import org.jahia.services.usermanager.JahiaUser;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.jahia.utils.PaginatedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class SubscriptionService {
    public static final String J_EMAIL = "j:email";
    public static final String J_FIRST_NAME = "j:firstName";
    public static final String J_LAST_NAME = "j:lastName";
    public static final String J_PROVIDER = "j:provider";
    public static final String J_SUBSCRIBER = "j:subscriber";
    public static final String J_SUBSCRIPTIONS = "j:subscriptions";
    public static final String JMCIM_LIST = "jmcim:List";
    public static final String JMCIM_CONFIG = "jmcimConfig";
    public static final String JMCIM_SUBSCRIPTION = "jmcim:subscription";
    public static final String J_TARGET = "j:target";
    public static final String BEAN_NAME = "jmcimSubscriptionService";

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private JahiaUserManagerService userManagerService;

    @Autowired
    MailChimpService mailChimpService;

    private Map<String, String> defaultFieldMap = new HashMap<String, String>();

    /**
     * Map MailChimps lists to use on site
     * @param ctx RenderContext
     * @param listIds
     * @throws RepositoryException
     * @throws IOException
     * @throws MailChimpException
     */
    public void mapList(org.jahia.services.render.RenderContext ctx, String[] listIds) throws RepositoryException, IOException, MailChimpException {
        JCRNodeWrapper node=ctx.getMainResource().getNode();
        MailChimpSettings settings = mailChimpService.getSettings(node);
        for (String listId : listIds) {
            JCRNodeWrapper lists = settings.getListsNode();
            try {
                settings.getListNode(listId);
            } catch (RepositoryException e) {
            }
            JCRNodeWrapper newList = lists.addNode(listId, JMCIM_LIST);
            ;
            ListMethodResult listRes = settings.getAPI().getLists(listId);
            if (listRes.data.size()>0) {
                ListMethodResult.Data listProp = listRes.data.get(0);
                newList.setProperty("webId", listProp.web_id);
                newList.setProperty("jcr:title", listProp.name);
                newList.setProperty("subscribeUrlShort", listProp.subscribe_url_long);
                newList.setProperty("emailTypeOption", listProp.email_type_option);
                JCRNodeWrapper newFields = newList.addNode("j:fields", "jmcim:varFields");
                for (String key : defaultFieldMap.keySet()) {
                    JCRNodeWrapper newField = newFields
                            .addNode(JCRContentUtils.findAvailableNodeName(newFields, "field"), "jmcim:varField");
                    newField.setProperty("j:key", key);
                    newField.setProperty("j:value", defaultFieldMap.get(key));
                }
            }
            lists.getSession().save();
        }
    }

    /**
     * Subscribe user in MailChimp list
     * @param user
     * @param listsNode
     * @throws MailChimpSubscriptionException
     */
    public void subscribe(final JahiaUser user,final JCRNodeWrapper listsNode) throws MailChimpSubscriptionException {
        if (JahiaUserManagerService.isGuest(user)) {
            throw new MailChimpSubscriptionException(MailChimpSubscriptionException.Status.unsupported_user);
        }
        String userEmail = user.getProperty(SubscriptionService.J_EMAIL);
        if (userEmail == null || !MailService.isValidEmailAddress(userEmail, false)) {
            // no valid e-mail provided -> refuse
            throw new MailChimpSubscriptionException(MailChimpSubscriptionException.Status.no_valid_email);
        }
        try {
            MailChimpSettings settings = mailChimpService.getSettings(listsNode);
            HashMap<String,String> vars=getUserVarsField(user, listsNode);
            String welcome = listsNode.getPropertyAsString(MailChimpSettings.LIST_WELCOME_OPTIN);
            final Email result = settings.getAPI()
                    .subscribe(listsNode.getName(),
                               userEmail,
                               vars,
                               "confirmation".equals(welcome),
                               "welcome".equals(welcome),
                               true);
            JCRNodeWrapper newSubscriptionNode = JCRTemplate.getInstance().doExecuteWithSystemSession("root", "live", new JCRCallback<JCRNodeWrapper>() {
                @Override
                public JCRNodeWrapper doInJCR(JCRSessionWrapper session) throws RepositoryException {
                    return createSubscription(session, session.getNodeByIdentifier(listsNode.getIdentifier()), user,
                                              result.email, result.euid, result.leid);
                }
            });
            if (newSubscriptionNode==null)
                throw new MailChimpSubscriptionException(MailChimpSubscriptionException.Status.error_subscribe);

        } catch (Exception e) {
            throw new MailChimpSubscriptionException(e,MailChimpSubscriptionException.Status.error_subscribe);
        }
    }

    public JCRNodeWrapper getSubscription(JCRNodeWrapper listsNode, String subscribeId) throws MailChimpSubscriptionException {
        try {
            if(!listsNode.isNodeType(JMCIM_LIST))
                return null;
            JCRNodeWrapper subscriptionsNode = listsNode.getNode(J_SUBSCRIPTIONS);
            for (JCRNodeWrapper subs : subscriptionsNode.getNodes()) {
                if(subs.isNodeType(JMCIM_SUBSCRIPTION) && subscribeId.equals(subs.getPropertyAsString("jmcim:euid"))){
                    return subs;
                }
            }

        } catch (RepositoryException e) {
            logger.error("JCR Error ",e);
        }
        return null;
    }

    public void unsubscribe(JCRSessionWrapper session,JCRNodeWrapper listsNode, String subscribeId) throws MailChimpSubscriptionException {
        try {
            JCRNodeWrapper subs = getSubscription(listsNode,subscribeId);
            if(null==subs)
                return;
            subs.remove();
            session.save();
        } catch (RepositoryException e) {
            logger.error("JCR Error ",e);
        }
    }
    /**
     * Unsubscribe user from MailChimp List
     * @param listsNode
     * @param user
     */
    public void unsubscribe(JCRNodeWrapper listsNode, JahiaUser user) throws MailChimpSubscriptionException {
        try {
            JCRSessionWrapper session = listsNode.getSession();
            String identifier = listsNode.getIdentifier();

            final JCRNodeWrapper subscriptionNode = getSubscription(listsNode, user, session);
            if (subscriptionNode == null) {
                throw new MailChimpSubscriptionException(MailChimpSubscriptionException.Status.invalid_user);
            }

            Email email=new Email();
            email.email=subscriptionNode.getPropertyAsString("jmcim:email");
            email.euid=subscriptionNode.getPropertyAsString("jmcim:euid");
            email.leid=subscriptionNode.getPropertyAsString("jmcim:leid");
            if(null==email.email||email.email.length()<1)
                email.email = user.getProperty(SubscriptionService.J_EMAIL);

            MailChimpSettings settings = mailChimpService.getSettings(listsNode);
            settings.getAPI().unsubscribe(listsNode.getName(), email);
            subscriptionNode.remove();
            session.save();
        } catch (MailChimpSubscriptionException e) {
            throw e;
        } catch (Exception e) {
            throw new MailChimpSubscriptionException(MailChimpSubscriptionException.Status.error_subscribe);
        }
    }


    /**
     * Retrieves subscription for the specified node.
     *
     * @param subscribableIdentifier the UUID of the subscribable node
     * @param orderBy                order by property; <code>null</code> if no sorting should be
     *                               done
     * @param orderAscending         do we sort in ascending direction?
     * @param offset                 the index of the first result to start with; <code>0</code> to
     *                               start from the beginning
     * @param limit                  the maximum number of results to return; <code>0</code> to
     *                               return all available
     * @param session
     * @return paginated list list of {@link PaginatedList<Subscription>} objects
     */
    public PaginatedList<Subscription> getSubscriptions(final String subscribableIdentifier, final String orderBy,
                                                        final boolean orderAscending, final int offset, final int limit,
                                                        JCRSessionWrapper session) {
        return getSubscriptions(subscribableIdentifier, false, false, orderBy, orderAscending, offset, limit, session);
    }

    /**
     * Retrieves subscription for the specified node.
     *
     * @param subscribableIdentifier the UUID of the subscribable node
     * @param activeOnly             return only non-suspended subscriptions
     * @param confirmedOnly          return only confirmed subscriptions
     * @param orderBy                order by property; <code>null</code> if no sorting should be
     *                               done
     * @param orderAscending         do we sort in ascending direction?
     * @param offset                 the index of the first result to start with; <code>0</code> to
     *                               start from the beginning
     * @param limit                  the maximum number of results to return; <code>0</code> to
     *                               return all available
     * @param session
     * @return paginated list list of {@link PaginatedList<Subscription>} objects
     */
    public PaginatedList<Subscription> getSubscriptions(final String subscribableIdentifier, final boolean activeOnly, final boolean confirmedOnly, final String orderBy,
                                                        final boolean orderAscending, final int offset, final int limit,
                                                        JCRSessionWrapper session) {

        long timer = System.currentTimeMillis();

        int total = 0;
        final List<Subscription> subscriptions = new LinkedList<Subscription>();
        try {
            JCRNodeWrapper target = session.getNodeByIdentifier(subscribableIdentifier);
            if (!target.isNodeType(JMCIM_LIST)) {
                logger.warn("The target node {} does not have the "+JMCIM_LIST+" mixin."+
                                    " No subscriptions can be found.", target.getPath());
                return new PaginatedList<Subscription>(subscriptions, 0);
            }
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            if (queryManager==null) {
                logger.error("Unable to obtain QueryManager instance");
                return new PaginatedList<Subscription>(subscriptions, 0);
            }

            StringBuilder q = new StringBuilder();
            q.append("select * from ["+JMCIM_SUBSCRIPTION+"] where isdescendantnode([").append(target.getPath())
                    .append("/"+J_SUBSCRIPTIONS+"])");
            if (orderBy!=null) {
                q.append(" order by [").append(orderBy).append("]").append(orderAscending ? "asc" : "desc");
            }
            Query query = queryManager.createQuery(q.toString(), Query.JCR_SQL2);

            if (limit>0 || offset>0) {
                total = (int)JCRContentUtils.size(query.execute().getNodes());
            }

            query.setLimit(limit);
            query.setOffset(offset);

            for (NodeIterator nodes = query.execute().getNodes(); nodes.hasNext(); ) {
                JCRNodeWrapper subscriptionNode = (JCRNodeWrapper)nodes.next();
                subscriptions.add(toSubscription(subscriptionNode, session));
            }
        } catch (RepositoryException e) {
            logger.error("Error retrieving subscriptions for node "+subscribableIdentifier, e);
        }

        if (logger.isDebugEnabled()) {
            logger.info("Subscriber search took "+(System.currentTimeMillis()-timer)+" ms. Returning "+
                                subscriptions.size()+" subscriber(s)");
        }

        return new PaginatedList<Subscription>(subscriptions, limit>0 || offset>0 ? total : subscriptions.size());
    }


    /**
     * Checks if the provided user is subscribed to the specified node.
     *
     * @param subscribableIdentifier the UUID of the target subscribable node
     * @param user                   the user key for the registered users or an e-mail for
     *                               non-registered users
     * @param session
     * @return <code>true</code> if the provided user is subscribed to the
     * specified node
     */
    public String getSubscription(final String subscribableIdentifier, final JahiaUser user, JCRSessionWrapper session) {
        try {
            JCRNodeWrapper target = session.getNodeByIdentifier(subscribableIdentifier);
            if (!target.isNodeType(JMCIM_LIST)) {
                logger.warn("The target node {} does not have the "+JMCIM_LIST+" mixin."+
                                    " No subscriptions can be found.", target.getPath());
                return null;
            }

            JCRNodeWrapper sub = getSubscription(target, user, session);
            if (sub!=null) {
                return sub.getIdentifier();
            }
            return null;
        } catch (RepositoryException e) {
            logger.error(
                    "Error checking subscription status for user '"+user+"' and node "+subscribableIdentifier, e);
        }
        return null;
    }

    /**
     * Checks if the provided user is subscribed to the specified node.
     *
     * @param target  the path of the target subscribable node
     * @param user    the user key for the registered users or an e-mail for
     *                non-registered users
     * @param session the JCR session
     * @return <code>true</code> if the provided user is subscribed to the
     * specified node
     * @throws javax.jcr.RepositoryException         in case of a JCR error
     * @throws javax.jcr.query.InvalidQueryException if the query syntax is invalid
     */
    public JCRNodeWrapper getSubscription(JCRNodeWrapper target, JahiaUser user, JCRSessionWrapper session)
            throws RepositoryException {
        QueryManager queryManager = session.getWorkspace().getQueryManager();
        if (queryManager==null) {
            logger.error("Unable to obtain QueryManager instance");
            return null;
        }
        String subscriber = user.getUsername();
        String provider = null;

        StringBuilder q = new StringBuilder(64);
        q.append("select * from ["+JMCIM_SUBSCRIPTION+"] where ["+J_SUBSCRIBER+"]='").append(subscriber)
                .append("'");
        if (provider!=null) {
            q.append(" and ["+J_PROVIDER+"]='").append(provider).append("'");
        }
        q.append(" and").append(" isdescendantnode('").append(target.getPath()).append("')");
        Query query = queryManager.createQuery(q.toString(), Query.JCR_SQL2);
        query.setLimit(1);
        final NodeIterator nodeIterator = query.execute().getNodes();
        if (nodeIterator.hasNext()) {
            return (JCRNodeWrapper)nodeIterator.nextNode();
        }
        return null;
    }

    protected void storeProperties(JCRNodeWrapper newSubscriptionNode, Map<String, Object> properties,
                                   JCRSessionWrapper session) {
        if (properties==null || properties.isEmpty()) {
            return;
        }

        for (Map.Entry<String, Object> property : properties.entrySet()) {
            if (!property.getValue().getClass().isArray()) {
                try {
                    newSubscriptionNode.setProperty(property.getKey(),
                                                    JCRContentUtils.createValue(property.getValue(), session.getValueFactory()));
                } catch (RepositoryException e) {
                    logger.warn("Unable to set property "+property.getKey(), e);
                }
            } else {
                logger.warn("Cannot handle nultivalue properties. Skipping property {}", property.getKey());
            }
        }
    }

    /**
     * Creates subscription for the specified users and subscribable node
     *
     * @param list                   list node
     * @param subscribers            a map with subscriber information. The key is a subscriber ID,
     *                               the value is a map with additional properties that will be
     *                               stored for the subscription object. The subscriber ID is a a
     *                               user key in case of a registered Jahia user (the one, returned
     *                               by {@link org.jahia.services.usermanager.JahiaUser#getUserKey()}). In case of a
     *                               non-registered user this is an e-mail address of the
     */
    public JCRNodeWrapper createSubscription(JCRSessionWrapper session,final JCRNodeWrapper list,
                                             final Map<JahiaUser, Map<String, Object>> subscribers) {

        JCRNodeWrapper newSubscriptionNode = null;
        try {
            if (!list.isNodeType(JMCIM_LIST)) {
                logger.warn("The target node {} does not have the "+JMCIM_LIST+" mixin."+
                                    " No subscriptions can be created.", list.getPath());
                return null;
            }
            JCRNodeWrapper subscriptionsNode = list.getNode(J_SUBSCRIPTIONS);
            String targetPath = subscriptionsNode.getPath();
            if (list.isLocked() || subscriptionsNode.isLocked()) {
                logger.info("The target {} is locked and no subscriptions can be created. Skipping {} subscribers.",
                            targetPath, subscribers.size());
            }


            for (Map.Entry<JahiaUser, Map<String, Object>> subscriber : subscribers.entrySet()) {
                JahiaUser jahiaUser = subscriber.getKey();
                String provider = jahiaUser.getProviderName();
                if (getSubscription(list, subscriber.getKey(), session)==null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Creating subscription to the {} for {}.", targetPath, subscriber.getKey());
                    }
                    session.checkout(subscriptionsNode);
                    newSubscriptionNode = subscriptionsNode
                            .addNode(JCRContentUtils.findAvailableNodeName(subscriptionsNode, "subscription"),
                                     JMCIM_SUBSCRIPTION);
                    newSubscriptionNode.setProperty(J_SUBSCRIBER, jahiaUser.getUsername());
                    if (provider!=null) {
                        newSubscriptionNode.setProperty(J_PROVIDER, provider);
                    }
                    storeProperties(newSubscriptionNode, subscriber.getValue(), session);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Subscription for the {} and {} is already present. Skipping ceraring new one.",
                                     targetPath, subscriber.getKey());
                    }
                }
            }
            session.save();
        } catch (RepositoryException e) {
            logger.error("Error creating subscriptions for node "+list, e);
        }
        return newSubscriptionNode;
    }

    /**
     * Creates subscription for the specified user and subscribable node
     * @param list                   list node
     * @param jahiaUser                the key of a registered Jahia user (the one, returned by
     *                               {@link org.jahia.services.usermanager.JahiaUser#getUserKey()})
     * @param email
     * @param euid
     * @param leid
     */
    public JCRNodeWrapper createSubscription(JCRSessionWrapper session,final JCRNodeWrapper list, JahiaUser jahiaUser, String email, String euid, String leid) {
        Map<JahiaUser, Map<String, Object>> subscribers = new HashMap<JahiaUser, Map<String, Object>>(1);
        Map<String, Object> props = new HashMap<String, Object>(1);
        props.put("jmcim:email", email);
        props.put("jmcim:euid", euid);
        props.put("jmcim:leid", leid);
        if(null==jahiaUser){
            HashSet<String>emailList=new HashSet<String>();
            emailList.add(email);
            HashMap<String, JahiaUser> users=getUsersByEmails(emailList);
            if(!users.containsKey(email))
                return null;
            jahiaUser=users.get(email);
        }
        subscribers.put(jahiaUser, props);
        return createSubscription(session,list, subscribers);
    }


    protected Subscription toSubscription(JCRNodeWrapper subscriptionNode, JCRSessionWrapper session)
            throws RepositoryException {
        Subscription subscriber = new Subscription();

        subscriber.setId(subscriptionNode.getIdentifier());
        subscriber.setSubscriber(subscriptionNode.getProperty(J_SUBSCRIBER).getString());

        String provider = null;
        try {
            provider = subscriptionNode.getProperty(J_PROVIDER).getString();
            subscriber.setProvider(provider);
        } catch (PathNotFoundException e) {
            // non-registered subscriber
        }

        if (provider!=null) {
            // registered user
            String key = "{"+provider+"}"+subscriber.getSubscriber();
            JahiaUser user = userManagerService.lookup(key).getJahiaUser();
            if (user!=null) {
                subscriber.setFirstName(user.getProperty(J_FIRST_NAME));
                subscriber.setLastName(user.getProperty(J_LAST_NAME));
                subscriber.setEmail(user.getProperty(J_EMAIL));
            } else {
                logger.warn("Unable to find user for key {}", key);
            }
        } else {
            subscriber.setEmail(subscriber.getSubscriber());
            try {
                subscriber.setFirstName(subscriptionNode.getProperty(J_FIRST_NAME).getString());
            } catch (PathNotFoundException e) {
                // no property set
            }
            try {
                subscriber.setLastName(subscriptionNode.getProperty(J_LAST_NAME).getString());
            } catch (PathNotFoundException e) {
                // no property set
            }
        }


        subscriber.getProperties().putAll(subscriptionNode.getPropertiesAsString());

        return subscriber;
    }


    /**
     * Validate and synchronize MailChimp lists for site
     * @param session
     * @param settingsNode
     * @throws RepositoryException
     */
    public void syncLists(JCRSessionWrapper session, JCRNodeWrapper settingsNode) throws RepositoryException {
        MailChimpSettings settings = mailChimpService.getSettings(settingsNode);
        MailChimpAPI api= settings.getAPI();
        try {
            MailChimpJahiaList listRes = settings.getListsSync();
            if (listRes.getNoLinked().size()>0) {
                JCRNodeWrapper lists = settings.getListsNode();
                session.checkout(lists);
                for (String listId : listRes.getNoLinked()) {
                    JCRNodeWrapper listNode=lists.getNode(listId);
                    api.webhookDel(listId,listNode.getPropertyAsString("webhookUrl"));
                    listNode.remove();
                }
                session.save();
            }
            for (ListMethodResult.Data data : listRes.getLocal()) {
                syncVarFields(data.id, session, settings);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MailChimpException e) {
            e.printStackTrace();
        }
    }

    private void syncVarFields(String listId, JCRSessionWrapper session, MailChimpSettings settings) throws RepositoryException, IOException, MailChimpException {

        MailChimpJahiaMergeVars fields = settings.getMergeVarsSync(listId);
        if (fields.getNoLinked().size()<1) {
            return;
        }
        JCRNodeWrapper varFieldsNode = settings.getListsNode().getNode("/j:fields");
        session.checkout(varFieldsNode);
        for (JCRNodeWrapper varFieldNode : varFieldsNode.getNodes()) {
            if (fields.getNoLinked().containsKey(varFieldNode.getPropertyAsString("j:key"))) {
                varFieldNode.remove();
            }
        }
        session.save();
    }

    public void syncSubscribers(JCRNodeWrapper node) throws RepositoryException, IOException, MailChimpException {
        MailChimpSettings settings = mailChimpService.getSettings(node);

        MailChimpJahiaList listRes = settings.getListsSync();
        for (ListMethodResult.Data list : listRes.getLocal()) {
            syncSubscribers(list.id, settings);

        }

    }

    private void syncSubscribers(String listId, MailChimpSettings settings) throws RepositoryException, IOException, MailChimpException {
        JCRSessionWrapper session = settings.getNode().getSession();
        JCRNodeWrapper subscriptionsNode = settings.getListsNode().getNode(listId+"/j:subscriptions");
        HashMap<String, JCRNodeWrapper> subsNodes = new HashMap<String, JCRNodeWrapper>();
        for (JCRNodeWrapper subscriber : subscriptionsNode.getNodes()) {
            subsNodes.put(subscriber.getPropertyAsString("jmcim:email"), subscriber);
        }

        MembersResult mir = settings.getAPI().getMembers(listId, "subscribed");

        HashMap<String, JCRNodeWrapper> activeSubsEmail = new HashMap<String, JCRNodeWrapper>();
        HashMap<String, MemberInfoData> remoteSubsEmail = new HashMap<String, MemberInfoData>();
        for (MemberInfoData memberInfoData : mir.data) {
            //subscraption exist
            if (subsNodes.containsKey(memberInfoData.email)) {
                activeSubsEmail.put(memberInfoData.email, subsNodes.remove(memberInfoData.email));
            } else {
                //remote subscribers
                remoteSubsEmail.put(memberInfoData.email, memberInfoData);
            }
        }
        //Clean subscraptions.
        session.checkout(subscriptionsNode);
        for (String email : subsNodes.keySet()) {
            subsNodes.get(email).remove();
        }
        if (subsNodes.size()>0) {
            session.save();
        }
        //fields update for activeSubs
        for (String email : activeSubsEmail.keySet()) {
            String username = activeSubsEmail.get(email).getPropertyAsString("j:subscriber");
            updateMember(userManagerService.lookupUser(username).getJahiaUser(), subscriptionsNode.getParent(), settings);
        }
        HashMap<String, JahiaUser> localUsers = getUsersByEmails(remoteSubsEmail.keySet());
        for (String email : localUsers.keySet()) {
            createSubscription(session,subscriptionsNode.getParent(),
                               localUsers.get(email),
                               email,
                               remoteSubsEmail.get(email).leid,
                               "");
        }

    }

    private Email updateMember(JahiaUser user, JCRNodeWrapper listsNode, MailChimpSettings settings) throws RepositoryException, IOException, MailChimpException {
        return settings.getAPI().
                updateMember(
                        listsNode.getName(),
                        user.getProperty("j:email"),
                        getUserVarsField(user, listsNode)
                );

    }

    private HashMap<String, String> getUserVarsField(JahiaUser user, JCRNodeWrapper listsNode) throws RepositoryException {
        HashMap<String, String> vars = new HashMap<String, String>();
        JCRNodeWrapper fieldsNode = listsNode.getNode("j:fields");

        for (JCRNodeWrapper fieldNode : fieldsNode.getNodes()) {
            if (fieldNode.isNodeType("jmcim:varField")) {
                vars.put(fieldNode.getPropertyAsString("j:key"), user.getProperty(fieldNode.getPropertyAsString("j:value")));
            }
        }
        return vars;
    }

    private HashMap<String, JahiaUser> getUsersByEmails(Set<String> emails) {
        HashMap<String, JahiaUser> result = new HashMap<String, JahiaUser>();

        for (String userKey : userManagerService.getUserList()) {
            JahiaUser user = userManagerService.lookup(userKey).getJahiaUser();
            String email = user.getProperty("j:email");
            if (emails.contains(email)) {
                result.put(email, user);
            }
        }

        return result;
    }

    public void setUserManagerService(JahiaUserManagerService userManagerService) {
        this.userManagerService = userManagerService;
    }

    public Map<String, String> getDefaultFieldMap() {
        return defaultFieldMap;
    }

    public void setDefaultFieldMap(Map<String, String> defaultFieldMap) {
        this.defaultFieldMap = defaultFieldMap;
    }

}

