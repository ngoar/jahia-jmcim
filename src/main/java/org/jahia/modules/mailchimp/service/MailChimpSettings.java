/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.MailChimpObject;
import com.ecwid.mailchimp.method.v2_0.lists.ListMethodResult;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.lists.MailChimpJahiaMergeVars;
import org.jahia.modules.mailchimp.api.lists.MergeVarsResult;
import org.jahia.modules.mailchimp.api.lists.WebHookResult;
import org.jahia.modules.mailchimp.service.exceptions.MailChimpNotConfiguredException;
import org.jahia.modules.mailchimp.service.model.MailChimpJahiaList;
import org.jahia.modules.mailchimp.sitesettings.model.WebHookModel;
import org.jahia.services.content.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * MailChimp Settings object.
 */
public class MailChimpSettings {
    private static final Logger logger = LoggerFactory.getLogger(MailChimpSettings.class);

    public static final String API_KEY = "apiKey";
    public static final String TEMPLATES_NODE_NAME = "templates";
    public static final String SECURE_NODE_NAME="secure";
    public static final String LISTS_NODE_NAME = "lists";
    public static final String CAMPAIGNS_NODE_NAME = "campaigns";
    public static final String API_KEY_VALID = "apiKeyValid";
    public static final String API_KEY_VALID_DATE = "apiKeyValidDate";
    public static final String ACCOUNT_UUID = "accountUuid";
    public static final String LIST_WELCOME_OPTIN = "welcome";


    private MailChimpService mailChimpService;
    private JCRNodeWrapper settings;
    private String apiKey;
    private MailChimpAPI api;
    private String accoutUuid;
    private String listRootPath;

    /**
     * Create by MailChimpService
     */
    MailChimpSettings(MailChimpService mailChimpService) {
        this.mailChimpService = mailChimpService;
    }

    /**
     * Create by MailChimpService
     */
    MailChimpSettings(final JCRNodeWrapper settings, MailChimpService mailChimpService){
        this(mailChimpService);
        if (settings!=null) {
            this.settings = settings;
            try {
                apiKey = JCRTemplate.getInstance().doExecuteWithSystemSession(new JCRCallback<String>() {
                    @Override
                    public String doInJCR(JCRSessionWrapper session) throws RepositoryException {
                        listRootPath=settings.getNode(LISTS_NODE_NAME).getPath();

                        return getSecureNode(session).getPropertyAsString(API_KEY);
                    }
                });
            } catch (RepositoryException e) {
                apiKey=null;
                logger.error("Can't read MailChimp api key in JCR");
            }
            accoutUuid = settings.getPropertyAsString(ACCOUNT_UUID);
            if (null!=apiKey) {
                apiKey = apiKey.trim();
            }
        }
    }

    private JCRNodeWrapper getSecureNode(JCRSessionWrapper session) throws RepositoryException {
        JCRNodeWrapper sessionNodeByIdentifier = session.getNodeByIdentifier(settings.getIdentifier());
        return sessionNodeByIdentifier.getNode(SECURE_NODE_NAME);
    }

    public boolean isActive() {
        try {
            if (apiKey==null) {
                return false;
            }
            return settings.getProperty("apiKeyValid").getBoolean();
        } catch (Exception e) {
            logger.error("Can't check key status", e);
            return false;
        }
    }

    public String getApiKey() {
        return apiKey;
    }

    private void checkActive() throws MailChimpNotConfiguredException {
        if (!isActive()) {
            throw new MailChimpNotConfiguredException();
        }
    }

    public MailChimpAPI getAPI() {
        checkActive();
        if (api==null) {
            api = mailChimpService.createAPI(apiKey);
        }
        return api;
    }

    public JCRNodeWrapper getTemplatesNode() throws RepositoryException {
        return settings.getNode(TEMPLATES_NODE_NAME);
    }

    public JCRNodeWrapper getListsNode() throws RepositoryException {
        return settings.getNode(LISTS_NODE_NAME);
    }

    public String getListRootPath() {
        return listRootPath;
    }

    public JCRNodeWrapper getCampaignsNode() throws RepositoryException {
        return settings.getNode(CAMPAIGNS_NODE_NAME);
    }

    public JCRNodeWrapper getListNode(String listId) throws RepositoryException {
        return getListsNode().getNode(listId);
    }

    public JCRNodeWrapper getNode() {
        return settings;
    }

    public JCRNodeWrapper getNodeInSession(JCRSessionWrapper session) throws RepositoryException {
        return session.getNode(settings.getPath());
    }

    public String getApiPrefix() {
        if (apiKey==null) {
            return "";
        }
        return apiKey.substring(apiKey.indexOf("-")+1)+".";
    }

    public MailChimpJahiaList getListsSync() throws IOException, MailChimpException, RepositoryException {
        JCRNodeWrapper lists = getListsNode();
        HashSet<String> mappedLists = new HashSet<String>();
        for (JCRItemWrapper jcrItem : lists.getNodes()) {
            if ((jcrItem instanceof JCRNodeWrapper)
                    && ((JCRNodeWrapper)jcrItem).isNodeType(SubscriptionService.JMCIM_LIST)) {
                mappedLists.add(jcrItem.getName());
            }
        }

        ListMethodResult apiResult = getAPI().getLists(null);
        return new MailChimpJahiaList(apiResult.data, mappedLists);
    }

    public MailChimpJahiaMergeVars getMergeVarsSync(String listId) throws IOException, MailChimpException, RepositoryException {
        if (null==listId) {
            return new MailChimpJahiaMergeVars();
        }
        MergeVarsResult mcResult = getAPI().getMergeVars(listId);
        if (null==mcResult || null==mcResult.data || mcResult.data.size()<1) {
            return new MailChimpJahiaMergeVars();
        }
        ArrayList<MailChimpObject> merge_vars = (ArrayList<MailChimpObject>)mcResult.data.get(0).get("merge_vars");
        MailChimpJahiaMergeVars result = new MailChimpJahiaMergeVars(merge_vars);
        HashMap<String, String> lFields = new HashMap<String, String>();
        JCRNodeWrapper varFieldsNode = getListNode(listId).getNode("j:fields");
        for (JCRNodeWrapper varFieldNode : varFieldsNode.getNodes()) {
            lFields.put(varFieldNode.getPropertyAsString("j:key"), varFieldNode.getPropertyAsString("j:value"));
        }

        for (MailChimpJahiaMergeVars.MergeVarsLocal rMergeVars : result.getRemote()) {
            String tag = (String)rMergeVars.vars.get("tag");
            if (lFields.containsKey(tag)) {
                rMergeVars.key = tag;
                rMergeVars.value = lFields.get(tag);
                result.getLocal().add(rMergeVars);
                lFields.remove(tag);
            }
        }
        result.getNoLinked().putAll(lFields);
        return result;
    }

    public String getAccoutUuid() {
        return accoutUuid;
    }

    public List<WebHookResult> getWebHooks(String listId) throws IOException, MailChimpException {
        List<WebHookResult> result=new ArrayList<WebHookResult>();
        if (null!=listId) {
            result=getAPI().getWebHooks(listId);
        }
        return result;
    }

    public WebHookResult getWebHook(String listId, String url) throws IOException, MailChimpException {
        List<WebHookResult> result=getWebHooks(listId);
        if(null!=url)
            for (WebHookResult webHookResult : result) {
                if(url.equals(webHookResult.url))
                    return webHookResult;
            }
        return null;
    }

    public List<WebHookModel> getWebHooksModel(String listId) throws IOException, MailChimpException, RepositoryException {
        List<WebHookResult> resultApi=getWebHooks(listId);
        List<WebHookModel> result= new ArrayList<WebHookModel>();
        JCRNodeWrapper listNode=getListNode(listId);
        String webhookUrl=listNode.getPropertyAsString("webhookUrl");
        for (WebHookResult webHookResult : resultApi) {
            WebHookModel webhook=new WebHookModel(webHookResult);
            if(webhookUrl!=null && webhookUrl.equals(webhook.getUrl()))
                webhook.setSpecial(true);
            result.add(webhook);
        }

        return result;
    }

}