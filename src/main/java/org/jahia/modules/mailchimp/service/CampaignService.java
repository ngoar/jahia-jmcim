/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service;

import com.ecwid.mailchimp.MailChimpException;
import org.jahia.bin.listeners.JahiaContextLoaderListener;
import org.jahia.modules.mailchimp.api.MailChimpAPI;
import org.jahia.modules.mailchimp.api.campaigns.CampaignUpdateResult;
import org.jahia.modules.mailchimp.api.campaigns.list.CampaignListResult;
import org.jahia.modules.mailchimp.api.campaigns.list.Data;
import org.jahia.modules.mailchimp.sitesettings.model.CampaignDetails;
import org.jahia.modules.mailchimp.sitesettings.model.CampaignSetModel;
import org.jahia.services.content.*;
import org.jahia.services.render.RenderContext;
import org.jahia.services.scheduler.SchedulerService;
import org.quartz.CronTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailBean;

import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alexander Storozhuk on 06.05.2015.
 * Time: 16:04
 */
public class CampaignService implements InitializingBean, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(CampaignService.class);

    @Autowired
    private TemplateService templateService;

    private SchedulerService schedulerService;

    public static final String BEAN_NAME = "jmcimCampaignService";
    public static final String CAMPAIGNS_SET_TYPE = "jmcim:CampaignSet";
    public static final String CAMPAIGN_NODE_TYPE = "jmcim:Campaign";
    public static final String CAMPAIGN_TYPE = "regular";
    public static final String  CAMPAIGN_SET_KEY_ID = "cmSetId";
    public static final String JOB_GROUP_NAME = "MailChimpCampaignJob";
    private static final String CRON_TRIGGER_PREFIX = "Trigger_";
    private static final String JOB_DETAIL_PREFIX = "Job_";

    /**
     * Create or update campaign set
     *
     * @param session
     * @param renderContext
     * @param campModel
     * @throws RepositoryException
     * @throws IOException
     * @throws MailChimpException
     */
    public void createUpdateCampaignSet(JCRSessionWrapper session, RenderContext renderContext, CampaignSetModel campModel) throws RepositoryException, IOException, MailChimpException, ParseException {
        MailChimpSettings settings = templateService.getMailChimpService().getSettings(renderContext.getSite());
        JCRNodeWrapper campaignSetsList = settings.getCampaignsNode();
        JCRNodeWrapper contentNode = session.getNodeByUUID(campModel.getContentNode());
        String pageHtml = templateService.renderNodeExternally(renderContext, contentNode);
        MailChimpAPI api = settings.getAPI();

        JCRNodeWrapper listNode = session.getNodeByUUID(campModel.getListId());
        JCRNodeWrapper newCampaignSet;
        if (campModel.isUpdate()) {
            if(campModel.isWasSend()){
                throw new MailChimpException(313,"Sorry, you can not update this campaign at this time. It's status is: sent");
            }

            newCampaignSet = session.getNodeByUUID(campModel.getCampaignSetUUID());
            session.checkout(newCampaignSet);
        } else {
            String nodeName=JCRContentUtils.generateNodeName(campModel.getTitle());
            nodeName = JCRContentUtils.findAvailableNodeName(campaignSetsList, nodeName);
            newCampaignSet = campaignSetsList.addNode(nodeName, CAMPAIGNS_SET_TYPE);
            newCampaignSet.setProperty("type", CAMPAIGN_TYPE);
        }
        newCampaignSet.setProperty("jcr:title", campModel.getTitle());
        newCampaignSet.setProperty("subject", campModel.getSubject());
        newCampaignSet.setProperty("fromEmail", campModel.getFromEmail());
        newCampaignSet.setProperty("fromName", campModel.getFromName());
        newCampaignSet.setProperty("toName", campModel.getToName());
        newCampaignSet.setProperty("contentNode", contentNode);
        newCampaignSet.setProperty("list", listNode);
        newCampaignSet.setProperty("scheduler", campModel.getScheduler());
        newCampaignSet.setProperty("wasSend", campModel.isWasSend());
        newCampaignSet.setProperty("autoSend", campModel.isAutoSend());
        if ( campModel.getCleanPeriod() ==null)
            newCampaignSet.setProperty("cleanPeriod",(String)null);
        else
            newCampaignSet.setProperty("cleanPeriod", campModel.getCleanPeriod());

        final String tempateTile = campModel.getTitle()+" Template";
        if (campModel.isUpdate()) {
            String campaignId = newCampaignSet.getProperty("templateCampaignId").getString();
            try {
                CampaignUpdateResult campaignUpdateResult = api.updateCampaignOptions(campaignId, tempateTile, campModel.getSubject(),
                                                                                    campModel.getFromEmail(), campModel.getFromName(),
                                                                                    campModel.getToName(), listNode.getName());
            } catch (MailChimpException me) {
                if(313==me.code) {
                    newCampaignSet.setProperty("wasSend", true);
                    session.save();
                }
                throw me;
            }
        } else {
            CampaignListResult campaignListResult = api.addCampaign(tempateTile, CAMPAIGN_TYPE , campModel.getSubject(),
                    campModel.getFromEmail(), campModel.getFromName(), campModel.getToName(), listNode.getName(), pageHtml);
            newCampaignSet.setProperty("templateCampaignId", campaignListResult.get("id").toString());
            newCampaignSet.setProperty("templateCampaignWebId",(long) Double.parseDouble(campaignListResult.get("web_id").toString()));

        }

        session.save();
        try {
            if (null != newCampaignSet.getPropertyAsString("scheduler") && !newCampaignSet.getPropertyAsString("scheduler").trim().isEmpty())
                buildSchedulingBean(newCampaignSet).create();
            else buildSchedulingBean(newCampaignSet).destroy();
        } catch (ParseException pex) {
            throw new ParseException("Mistakes in cron scheduler expression. " + pex.getMessage(), pex.getErrorOffset());
        } catch (Exception e) {
            logger.error("Can`t create campaign job", e.getMessage());
        }

    }

    /**
     * Delete local campaign set and campaign in MailChimp
     *
     * @param campaignSetNode
     * @throws RepositoryException
     * @throws MailChimpException
     * @throws IOException
     */
    public void deleteCampaignSet(JCRNodeWrapper campaignSetNode) throws RepositoryException, MailChimpException, IOException {
        JCRSessionWrapper session = campaignSetNode.getSession();
        MailChimpSettings mailChimp = templateService.getMailChimpService().getSettings(campaignSetNode);

        String campaignId = campaignSetNode.getProperty("templateCampaignId").getString();
        try {
            mailChimp.getAPI().deleteCampaign(campaignId);

        } catch (MailChimpException mex) {
            if (mex.code == 506 ) {
                logger.warn("MailChimp campaign " + campaignSetNode.getDisplayableName() + "was already deleted in MailChimp ");
            } else if (mex.code == 300) {
                logger.warn("MailChimp campaign " + campaignSetNode.getDisplayableName() + " Invalid Campaign ID ");
            } else {
                throw mex;
            }
        }
        try {
            buildSchedulingBean(campaignSetNode).destroy();
        } catch (Exception e) {
            logger.error("Can`t delete campaign job", e.getMessage());
        }
        campaignSetNode.remove();
        session.save();
    }

    /**
     * Update content (html) of Campaign Set in MailChimp campaign
     * @param campaignSetNode
     * @param renderContext
     * @throws RepositoryException
     * @throws MailChimpException
     * @throws IOException
     */
    public void  updateCampaignSet(JCRNodeWrapper campaignSetNode,  RenderContext renderContext) throws RepositoryException, MailChimpException, IOException {
        MailChimpAPI api = templateService.getMailChimpService().getSettings(campaignSetNode).getAPI();
        String cId = campaignSetNode.getProperty("templateCampaignId").getString();
        JCRNodeWrapper contentNode = (JCRNodeWrapper)campaignSetNode.getProperty("contentNode").getNode();
        String html = templateService.renderNodeExternally(renderContext, contentNode);
        try {
            api.updateCampaignContent(cId, html);
        }catch (MailChimpException me){
            logger.error("Can`t update campaign content in MailChimp", me.getMessage());
            checkSendCompainSet(campaignSetNode.getSession(), campaignSetNode);
            throw me;
        }
    }

    /**
     * Create campaign
     *
     * @param campaignSetNode
     * @throws RepositoryException
     */
    public void createCampaign(JCRNodeWrapper campaignSetNode, RenderContext renderContext) throws RepositoryException {
        JCRSessionWrapper session = campaignSetNode.getSession();
        MailChimpAPI api = templateService.getMailChimpService().getSettings(campaignSetNode).getAPI();
        JCRNodeWrapper contentNode = (JCRNodeWrapper) campaignSetNode.getProperty("contentNode").getNode();
        String pageHtml =  templateService.renderNodeExternally(renderContext, contentNode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            String title = campaignSetNode.getPropertyAsString("jcr:title")+" "+sdf.format(new Date());
            CampaignListResult campaignListResult = api.replicateCampaign(campaignSetNode.getPropertyAsString("templateCampaignId"));
            final String cId = campaignListResult.get("id").toString();
            api.updateCampaignTitle(cId,title);
            api.updateCampaignContent(cId, pageHtml);
            JCRNodeWrapper newCampaign = campaignSetNode.addNode(cId, CAMPAIGN_NODE_TYPE);
            newCampaign.setProperty("jcr:title", title);
            newCampaign.setProperty("mailchimpCid", cId);
            newCampaign.setProperty("webId", (long) Double.parseDouble(campaignListResult.get("web_id").toString()));
            session.save();
            if (campaignSetNode.getPropertyAsString("autoSend").equals("true")) {
                api.sendCampaign(cId);
                newCampaign.setProperty("wasSend", true);
            }
            session.save();
        } catch (MailChimpException me) {
            logger.error("Can`t create or send campaign in MailChimp", me.getMessage());
        } catch (IOException ioe) {
            logger.error("Can`t get or set property from campaign node", ioe.getMessage());
        }
    }

    public boolean checkSendCompainSet(JCRSessionWrapper session, JCRNodeWrapper campaignNode) throws RepositoryException, IOException, MailChimpException {
        MailChimpAPI api = templateService.getMailChimpService().getSettings(campaignNode).getAPI();
        String campId = campaignNode.getPropertyAsString("templateCampaignId");
        CampaignListResult campaignListResult = api.getCampaignsList(campId);
        for (Data dataItem : campaignListResult.data) {
            if("send".equals(dataItem.status)){
                session.checkout(campaignNode);
                campaignNode.setProperty("wasSend",true);
                session.save();
                return true;
            }
        }
        return false;
    }


    /**
     * Delete local campaign and campaign in MailChimp

     * @param campaignNode
     * @throws RepositoryException
     * @throws MailChimpException
     * @throws IOException
     */
    public void deleteCampaign(JCRNodeWrapper campaignNode) throws RepositoryException, MailChimpException, IOException {
        JCRSessionWrapper session = campaignNode.getSession();
        MailChimpSettings mailChimp = templateService.getMailChimpService().getSettings(campaignNode);
        String campaignId = campaignNode.getProperty("mailchimpCid").getString();
        try {
            mailChimp.getAPI().deleteCampaign(campaignId);
        } catch (MailChimpException mex) {
            if (mex.code == 506 && mex.message.contains("has already been deleted")) {
                logger.warn("MailChimp campaign " + campaignNode.getDisplayableName() + "was already deleted in MailChimp");
            } else {
                throw mex;
            }
        }
        campaignNode.remove();
        session.save();
    }

    /**
     * Send campaign manually
     * @param campaignNode
     * @throws RepositoryException
     * @throws MailChimpException
     * @throws IOException
     */
    public void  sendCampaign(JCRNodeWrapper campaignNode) throws RepositoryException, MailChimpException, IOException {
        MailChimpAPI api = templateService.getMailChimpService().getSettings(campaignNode).getAPI();
        String cId = campaignNode.getProperty("mailchimpCid").getString();
        api.sendCampaign(cId);
        campaignNode.setProperty("wasSend", true);
    }

    /**
     * Get campaigns details
     *
     * @param campaignSetNode
     * @return
     * @throws RepositoryException
     */
    public Collection<CampaignDetails> getCampaignsDetails(JCRNodeWrapper campaignSetNode) throws RepositoryException, MailChimpException, IOException {
        MailChimpAPI api = templateService.getMailChimpService().getSettings(campaignSetNode).getAPI();
        HashMap<String, CampaignDetails> campaignDetails = new HashMap<String, CampaignDetails>();
        TreeMap<Date, CampaignDetails> campaignDetailsSorted = new TreeMap<Date, CampaignDetails>();
        for (JCRNodeWrapper item : campaignSetNode.getNodes()) {
            if (item.getPrimaryNodeTypeName().equals("jmcim:Campaign")) {
                CampaignDetails details = new CampaignDetails(item);
                campaignDetails.put(details.getNode().getPropertyAsString("mailchimpCid"), details);
                campaignDetailsSorted.put(details.getNode().getCreationDateAsDate(), details);
            }
        }
        if (campaignDetails.size() > 0) {
            String campsId = campaignDetails.keySet().toString().replace(" ", "").replace("[", "").replace("]", "");
            CampaignListResult campaignListResult = api.getCampaignsList(campsId);
            for (Data dataItem : campaignListResult.data) {
                CampaignDetails details = campaignDetails.get(dataItem.id);
                details.setDataInfo(dataItem);
            }
        }
        return campaignDetailsSorted.descendingMap().values();
    }

    private CampaignSchedulingBean buildSchedulingBean(JCRNodeWrapper cmSetNode) throws ParseException, RepositoryException {
        CampaignSchedulingBean bean = new CampaignSchedulingBean();
        bean.setRamJob(false);
        CronTrigger trigger = new CronTrigger(CRON_TRIGGER_PREFIX + cmSetNode.getPath());
        if (null != cmSetNode.getPropertyAsString("scheduler") && !cmSetNode.getPropertyAsString("scheduler").trim().isEmpty())
            trigger.setCronExpression(cmSetNode.getPropertyAsString("scheduler"));
        bean.setTrigger(trigger);

        JobDetailBean jobDetail = new JobDetailBean();
        jobDetail.setName(JOB_DETAIL_PREFIX + cmSetNode.getResolveSite().getName()+"/"+cmSetNode.getName());
        jobDetail.setGroup(JOB_GROUP_NAME);
        jobDetail.setJobClass(CampaignSendJob.class);
        jobDetail.getJobDataMap().put(CAMPAIGN_SET_KEY_ID, cmSetNode.getIdentifier());

        bean.setJobDetail(jobDetail);
        bean.setOverwriteExisting(true);
        bean.setSchedulerService(getSchedulerService());
        return bean;
    }

    public SchedulerService getSchedulerService() {
        return schedulerService;
    }

    public void setSchedulerService(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    @Override
    public void destroy() throws Exception {
        if (JahiaContextLoaderListener.isRunning()) {
            unscheduleJobs();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        resheduleJobs();
    }

    private void unscheduleJobs() {
        try {
            logger.info("Unschedule MailChimp CampaignSet Jobs");
            JCRTemplate.getInstance().doExecuteWithSystemSession(new JCRCallback<Object>() {
                @Override
                public Object doInJCR(JCRSessionWrapper session) throws RepositoryException {
                    String query = "SELECT * FROM [" + CAMPAIGNS_SET_TYPE + "] AS campaigns ";
                    Query q = session.getWorkspace().getQueryManager().createQuery(query, Query.JCR_SQL2);
                    QueryResult qr = q.execute();
                    RowIterator rows = qr.getRows();
                    while (rows.hasNext()) {
                        Row campaignSet = rows.nextRow();
                        try {
                            buildSchedulingBean((JCRNodeWrapper)campaignSet.getNode()).destroy();
                        } catch (Exception e) {
                            logger.error("Can't unschedule MailChimp CampaignSet Jobs",e);
                        }
                    }
                    return null;
                }
            });
        } catch (RepositoryException e) {
            logger.error("Can't unschedule MailChimp CampaignSet Jobs",e);
        }
    }

    private void resheduleJobs() {
        try {
            JCRTemplate.getInstance().doExecuteWithSystemSession(new JCRCallback<Object>() {
                @Override
                public Object doInJCR(JCRSessionWrapper session) throws RepositoryException {
                    String query = "SELECT * FROM [" + CAMPAIGNS_SET_TYPE + "] AS campaigns ";
                    Query q = session.getWorkspace().getQueryManager().createQuery(query, Query.JCR_SQL2);
                    QueryResult qr = q.execute();
                    RowIterator rows = qr.getRows();
                    while (rows.hasNext()) {
                        Row campaignSet = rows.nextRow();
                        try {
                            final CampaignSchedulingBean campaignSchedulingBean = buildSchedulingBean(
                                    (JCRNodeWrapper)campaignSet.getNode());
                            campaignSchedulingBean.setOverwriteExisting(false);
                            campaignSchedulingBean.create();
                        } catch (Exception e) {
                            logger.error("Can't unschedule MailChimp CampaignSet Jobs",e);
                        }
                    }
                    return null;
                }
            });
        } catch (RepositoryException e) {
            logger.error("Can't reschedule MailChimp CampaignSet Jobs",e);
        }
    }

    /**
     * Delete old campaigns in campaingSet if cleanPeriod set
     * @param campSetNode
     * @return
     * @throws RepositoryException
     */
    public int deleteOldCampaigns(JCRNodeWrapper campSetNode) throws RepositoryException {
        int count=0;
        final Long cleanPeriod =  campSetNode.hasProperty("cleanPeriod")?campSetNode.getProperty("cleanPeriod").getLong():null;
        if (null!=cleanPeriod) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -1*cleanPeriod.intValue());
            for (JCRNodeWrapper item : campSetNode.getNodes()) {
                if (item.getPrimaryNodeTypeName().equals("jmcim:Campaign")) {
                    if (item.getCreationDateAsDate().before(calendar.getTime())) {
                        try {
                            deleteCampaign(item);
                            count++;
                        } catch (Exception e) {
                            logger.warn("Can't delete old campaigns");
                        }
                    }
                }
            }
        }

        return count;
    }
}


