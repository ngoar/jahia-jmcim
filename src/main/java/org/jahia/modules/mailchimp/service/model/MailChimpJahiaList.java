/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.service.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.ecwid.mailchimp.method.v2_0.lists.ListMethodResult;

/**
 * Created by: Boris
 * Date: 4/24/2015
 * Time: 3:39 PM
 */
public class MailChimpJahiaList {
    protected List<ListMethodResult.Data> remote = new ArrayList<ListMethodResult.Data>();
    protected List<ListMethodResult.Data> local = new ArrayList<ListMethodResult.Data>();
    protected List<String> noLinked = new ArrayList<String>();

    public MailChimpJahiaList(List<ListMethodResult.Data> listsFromAPI, HashSet<String> mappedLists) {
        for (ListMethodResult.Data data : listsFromAPI) {
            if (mappedLists.contains(data.id)) {
                local.add(data);
            } else {
                remote.add(data);
                noLinked.add(data.id);
            }
        }
    }

    public List<ListMethodResult.Data> getRemote() {
        return remote;
    }

    public int getRemoteSize() {
        return remote.size();
    }

    public void setRemote(List<ListMethodResult.Data> remote) {
        this.remote = remote;
    }

    public List<ListMethodResult.Data> getLocal() {
        return local;
    }

    public int getLocalSize() {
        return remote.size();
    }

    public void setLocal(List<ListMethodResult.Data> local) {
        this.local = local;
    }

    public List<String> getNoLinked() {
        return noLinked;
    }

    public void setNoLinked(List<String> noLinked) {
        this.noLinked = noLinked;
    }
}
