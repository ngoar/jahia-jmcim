/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api.campaigns.create;

import com.ecwid.mailchimp.MailChimpObject;
import org.jahia.modules.mailchimp.api.campaigns.list.Tracking;

import java.util.List;

/**
 * Created by Alexander Storozhuk on 05.05.2015.
 * Time: 17:09
 */
public class Options extends MailChimpObject {
    @Field
    public String list_id;
    @Field
    public String subject;
    @Field
    public String from_email;
    @Field
    public String from_name;
    @Field
    public String to_name;
    @Field
    public Integer template_id;
    @Field
    public Integer gallery_template_id;
    @Field
    public Integer base_template_id;
    @Field
    public Integer folder_id;
    @Field
    public Tracking tracking;
    @Field
    public String title;
    @Field
    public Boolean authenticate;
    @Field
    public Analytics analytics;
    @Field
    public Boolean auto_footer;
    @Field
    public Boolean inline_css;
    @Field
    public Boolean generate_text;
    @Field
    public Boolean auto_tweet;
    @Field
    public List<MailChimpObject> auto_fb_post;
    @Field
    public Boolean fb_comments;
    @Field
    public Boolean timewarp;
    @Field
    public Boolean ecomm360;
    @Field
    public CrmTracking crm_tracking;
}
