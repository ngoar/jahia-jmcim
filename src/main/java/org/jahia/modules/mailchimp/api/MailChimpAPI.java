/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api;

import com.ecwid.mailchimp.MailChimpClient;
import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.MailChimpMethod;
import com.ecwid.mailchimp.MailChimpObject;
import com.ecwid.mailchimp.connection.HttpClientConnectionManager;
import com.ecwid.mailchimp.method.v2_0.lists.*;
import org.jahia.modules.mailchimp.api.campaigns.*;
import org.jahia.modules.mailchimp.api.campaigns.create.CampaignCreateMethod;
import org.jahia.modules.mailchimp.api.campaigns.create.Content;
import org.jahia.modules.mailchimp.api.campaigns.create.Options;
import org.jahia.modules.mailchimp.api.campaigns.list.CampaignListMethod;
import org.jahia.modules.mailchimp.api.campaigns.list.CampaignListResult;
import org.jahia.modules.mailchimp.api.helpers.AccountDetailsMethod;
import org.jahia.modules.mailchimp.api.helpers.AccountDetailsResult;
import org.jahia.modules.mailchimp.api.lists.*;
import org.jahia.modules.mailchimp.api.ping.PingMethod;
import org.jahia.modules.mailchimp.api.ping.PingResult;
import org.jahia.modules.mailchimp.api.templates.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * MailChimp API
 */
public class MailChimpAPI {
    String apiKey;
    MailChimpClient mailChimpClient;
    String listId;

    public MailChimpAPI(String apiKey) {
        this.apiKey = apiKey;
        mailChimpClient = new MailChimpClient(new HttpClientConnectionManager());
    }

    public PingResult validate(String apiKey) {
        try {
            return mailChimpClient.execute(new PingMethod(apiKey));
        } catch (MailChimpException e) {
            return new PingResult(e.code,e.message);
        } catch (IOException e) {
            return new PingResult(-99,e.getMessage());
        }
    }

    public Boolean deleteTemplate(Integer templateId) throws IOException, MailChimpException {
        TemplateDelMethod method = new TemplateDelMethod(apiKey,templateId);
        return mailChimpClient.execute(method).complete;

    }

    public Integer addTemplate(String name, String html) throws IOException, MailChimpException {
        TemplateAddMethod method = new TemplateAddMethod(apiKey, name, html);
        return mailChimpClient.execute(method).template_id;
    }


    public Boolean unDeleteTemplate(Integer templateId) throws IOException, MailChimpException {
            TemplateUndelMethod method = new TemplateUndelMethod(apiKey, templateId);
            return mailChimpClient.execute(method).complete;
    }

    public Boolean updateTemplate(Integer template_id, String name, String html) throws IOException, MailChimpException {
        TemplateUpdateMethod updateMethod = new TemplateUpdateMethod(apiKey, template_id, name, html);
        return mailChimpClient.execute(updateMethod).complete;
    }

    public AccountDetailsResult getAccountDetails() throws IOException, MailChimpException {
        return mailChimpClient.execute(new AccountDetailsMethod(apiKey));
    }

    public ListMethodResult getLists(String listId) throws IOException, MailChimpException {
        ListMethod method = new ListMethod();
        method.apikey = apiKey;
        if (null != listId && listId.length() > 0) {
            method.filters = new ListMethod.Filters();
            method.filters.list_id = listId;
        }
        return mailChimpClient.execute(method);
    }

    public ListMethodResult getLists() throws IOException, MailChimpException {
        return getLists(null);
    }

    public List<TemplateListResult.User> getTemplatesList() throws IOException, MailChimpException {
        TemplateListMethod listMethod = new TemplateListMethod(apiKey, true);
        return mailChimpClient.execute(listMethod).user;
    }

    public MergeVarsResult getMergeVars(String listId) throws IOException, MailChimpException {
        MergeVarsMethod method = new MergeVarsMethod();
        method.apikey = apiKey;
        method.id.add(listId);
        return mailChimpClient.execute(method);
    }


    public MembersResult getMembers(String listId,String status) throws IOException, MailChimpException {
        MembersMethod method = new MembersMethod();
        method.apikey=apiKey;
        method.id=listId;
        method.status=status;
        return mailChimpClient.execute(method);
    }

    public Email updateMember(String listId, String email, HashMap<String, String> vars) throws IOException, MailChimpException {
        UpdateMemberMethod method=new UpdateMemberMethod();
        method.apikey=apiKey;
        method.id=listId;
        method.email=new Email();
        method.email.email=email;
        method.merge_vars=new MailChimpObject();
        method.merge_vars.putAll(vars);
        method.replace_interests=true;
        return mailChimpClient.execute(method);

    }
    public Email subscribe(String listId, String email, HashMap<String, String> vars, boolean double_optin, boolean send_welcome, boolean html) throws IOException, MailChimpException {
        SubscribeMethod method = new SubscribeMethod();
        method.apikey = apiKey;
        method.id = listId;
        method.email = new Email();
        method.email.email = email;
        method.update_existing = true;
        method.double_optin = double_optin;
        method.send_welcome = send_welcome;
        method.email_type = html?"html":"text";
        method.merge_vars = new MailChimpObject();
        method.merge_vars.putAll(vars);
        method.merge_vars.put("EMAIL", email);
        return mailChimpClient.execute(method);
    }

    public boolean unsubscribe(String listId, String emailStr, String euid) throws IOException, MailChimpException {
        Email email = new Email();
        email.email = emailStr;
        email.euid = euid;
        return unsubscribe(listId, email);
    }

    public boolean unsubscribe(String listId, Email email) throws IOException, MailChimpException {
        UnsubscribeMethod method = new UnsubscribeMethod();
        method.apikey = apiKey;
        method.id = listId;
        method.email = email;
        method.send_goodbye = false;
        method.delete_member = true;
        return mailChimpClient.execute(method).complete;
    }

    public List<WebHookResult> getWebHooks(String listId) throws IOException, MailChimpException {
        WebHooksMethod method=new WebHooksMethod();
        method.apikey = apiKey;
        method.id = listId;
        return  mailChimpClient.execute(method);
    }

    public boolean webhookDel(String listId, String webhookUrl) throws IOException, MailChimpException {
        WebHookDelMethod method=new WebHookDelMethod();
        method.apikey = apiKey;
        method.id = listId;
        method.url = webhookUrl;
        return mailChimpClient.execute(method).complete;
    }

    public boolean webhookAdd(String listId, WebHookResult webhook) throws IOException, MailChimpException {
        WebhookAddMethod method=new WebhookAddMethod();
        method.apikey = apiKey;
        method.id = listId;
        method.url = webhook.url;
        method.actions=webhook.actions;
        method.sources=webhook.sources;
        MailChimpObject result=mailChimpClient.execute(method);
        return result.containsKey("id");
    }
    /**
     * Create a new draft campaign to send.
     * You can not have more than 32,000 campaigns in your account.
     * @param title
     * @param type
     * @param subject
     * @param emailFrom
     * @param nameFrom
     * @param nameTo
     * @param listId
     * @param html
     * @return
     * @throws IOException
     * @throws MailChimpException
     */
    public CampaignListResult addCampaign(String title, String type, String subject, String emailFrom, String nameFrom,
                                          String nameTo, String listId, String html) throws IOException, MailChimpException {
        CampaignCreateMethod method = new CampaignCreateMethod(apiKey);
        //TO-DO
        method.type = type;
        method.options.title = title;
        method.options.subject = subject;
        method.options.list_id = listId;
        method.options.from_email = emailFrom;
        method.options.from_name = nameFrom;
        method.options.to_name = nameTo;
        method.content.html = html;
        return mailChimpClient.execute(method);
    }

    public boolean deleteCampaign(String cId) throws IOException, MailChimpException {
        CampaignDeleteMethod deleteMethod = new CampaignDeleteMethod(apiKey, cId);
        return mailChimpClient.execute(deleteMethod).complete;
    }

    public CampaignListResult replicateCampaign(String cId) throws IOException, MailChimpException {
        CampaignReplicateMethod replicateMethod = new CampaignReplicateMethod(apiKey, cId);
        return mailChimpClient.execute(replicateMethod);
    }


    public CampaignUpdateResult updateCampaignContent(String cId, String html) throws IOException, MailChimpException {
        Content content = new Content();
        content.html = html;
        CampaignsUpdateMethod updateMethod = new CampaignsUpdateMethod(apiKey, cId, "content", content);
        return mailChimpClient.execute(updateMethod);
    }

    public CampaignUpdateResult updateCampaignTitle(String cId, String title) throws IOException, MailChimpException {
        Options options = new Options();
        options.title = title;
        CampaignsUpdateMethod updateMethod = new CampaignsUpdateMethod(apiKey, cId, "options", options);
        return mailChimpClient.execute(updateMethod);
    }
    public CampaignUpdateResult updateCampaignOptions(String cId, String title, String subject, String emailFrom, String nameFrom,
                                              String nameTo, String listId) throws IOException, MailChimpException {
        Options options = new Options();
        options.title = title;
        options.subject = subject;
        options.list_id = listId;
        options.from_email = emailFrom;
        options.from_name = nameFrom;
        options.to_name = nameTo;
        CampaignsUpdateMethod updateMethod = new CampaignsUpdateMethod(apiKey, cId, "options", options);
        return mailChimpClient.execute(updateMethod);
    }

    public boolean sendCampaign(String cId) throws IOException, MailChimpException {
        CampaignSendMethod sendMethod = new CampaignSendMethod(apiKey, cId);
        return mailChimpClient.execute(sendMethod).complete;
    }

    public CampaignListResult getCampaignsList(String campsId) throws IOException, MailChimpException {
        CampaignListMethod listMethod = new CampaignListMethod(apiKey, campsId);
        return mailChimpClient.execute(listMethod);
    }

}