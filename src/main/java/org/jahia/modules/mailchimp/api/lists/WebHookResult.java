/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api.lists;

import com.ecwid.mailchimp.MailChimpObject;

import java.util.List;
import java.util.Map;

/**
 * Created by naf on 11.05.2015.
 */
public class WebHookResult extends MailChimpObject {

    @Field
    public String url;
    @Field
    public Actions actions;
    @Field
    public Sources sources;

    public WebHookResult() {
    }

    public WebHookResult(String url) {
        this.url = url;
        actions = new Actions();
        sources = new Sources();
    }
    public WebHookResult(String url,Map params) {
        this(url);
        actions.subscribe=params.containsKey("subscribe");
        actions.unsubscribe=params.containsKey("unsubscribe");
        actions.profile=params.containsKey("profile");
        actions.cleaned=params.containsKey("cleaned");
        actions.upemail=params.containsKey("upemail");
        actions.campaign=params.containsKey("campaign");
        sources.user=params.containsKey("user");
        sources.admin=params.containsKey("admin");
        sources.api=params.containsKey("api");

    }

    public static class Actions extends MailChimpObject{
        @Field
        public Boolean subscribe;
        @Field
        public Boolean unsubscribe;
        @Field
        public Boolean profile;
        @Field
        public Boolean cleaned;
        @Field
        public Boolean upemail;
        @Field
        public Boolean campaign;
    }
    public static class Sources extends MailChimpObject{
        @Field
        public Boolean user;
        @Field
        public Boolean admin;
        @Field
        public Boolean api;
    }
}
