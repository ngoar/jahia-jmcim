/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api.templates;

import com.ecwid.mailchimp.MailChimpAPIVersion;
import com.ecwid.mailchimp.MailChimpMethod;
import com.ecwid.mailchimp.MailChimpObject;

/**
 * Retrieve various templates available in the system, allowing some thing similar to our template gallery to be created.
 * Created by Alexander Storozhuk on 31.03.2015.
 * Time: 14:52
 */
@MailChimpMethod.Method(name = "templates/list", version = MailChimpAPIVersion.v2_0)
public class TemplateListMethod extends MailChimpMethod<TemplateListResult> {

    @Field
    public Types types;

    @Field
    public Filters filters;

    /**
     * Types - optional the types of templates to return
     */
    public static class Types extends MailChimpObject {

        @Field
        public Boolean user;

        @Field
        public Boolean gallery;

        @Field
        public Boolean base;
    }


    /**
     * Filters to control how inactive templates are returned, if at all  - all are optional:
     */
    public static class Filters extends MailChimpObject {

        @Field
        public String category;

        @Field
        public String folder_id;

        @Field
        public Boolean include_inactive;

        @Field
        public Boolean inactive_only;

        @Field
        public Boolean include_drag_and_drop;
    }

    public TemplateListMethod() {
    }

    public TemplateListMethod(String apiKey, boolean include_inactive) {
        this.apikey = apiKey;
        this.types = new Types();
        this.types.user = true;
        this.types.gallery = false;
        this.types.base = false;
        this.filters = new Filters();
        this.filters.include_inactive = include_inactive;
        this.filters.include_drag_and_drop = true;
    }


}
