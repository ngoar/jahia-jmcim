/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api;

import com.ecwid.mailchimp.connection.HttpClientConnectionManager;
import com.ecwid.mailchimp.connection.MailChimpConnectionManager;
import com.ecwid.mailchimp.internal.gson.MailChimpGsonFactory;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Low level API call
 * Created by naf on 31.03.2015.
 */
public class ExecJsonMethod {

    private final MailChimpConnectionManager connection;
    private String apiKey;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    private static final Logger logger = LoggerFactory.getLogger(ExecJsonMethod.class);
    private final Gson gson = MailChimpGsonFactory.createGson();

    public ExecJsonMethod(String apiKey) {
        connection=new HttpClientConnectionManager();
        setApiKey(apiKey);
    }
    public String execute(String method, JSONObject param) throws IOException,JSONException {
        String url = buildUrl(apiKey, method);
        param.put("apikey",apiKey);
        String request = param.toString();
        logger.debug("Post to "+url+" : "+request);
        String response = connection.post(url, request);
        logger.debug("Response: " + response);
        return response;

    }
    public JsonElement executeGson(String method, JSONObject param) throws IOException,JSONException {
        return new JsonParser().parse(execute(method,param));
    }
    public JSONObject executeJson(String method, JSONObject param) throws IOException,JSONException {
        return new JSONObject(execute(method,param));
    }

    private String buildUrl(String apikey,String method) throws UnsupportedEncodingException {
        if(apikey == null) throw new IllegalArgumentException("apikey is not set");
        String prefix;
        int dash = apikey.lastIndexOf('-');
        if(dash > 0 && dash<apikey.length()-3 ) {
            prefix = apikey.substring(dash + 1);
            if(!prefix.equals( URLEncoder.encode(prefix,"UTF-8")))
                throw new IllegalArgumentException("Wrong apikey: "+apikey);
        } else {
            throw new IllegalArgumentException("Wrong apikey: "+apikey);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("https://").append(prefix).append(".api.mailchimp.com/2.0/").append(method).append(".json");
        return sb.toString();
    }


}
