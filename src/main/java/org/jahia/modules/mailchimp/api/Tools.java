/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api;

import java.io.IOException;

import com.ecwid.mailchimp.MailChimpClient;
import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.MailChimpMethod;
import com.ecwid.mailchimp.connection.HttpClientConnectionManager;
import com.ecwid.mailchimp.internal.gson.MailChimpGsonFactory;
import com.ecwid.mailchimp.method.v1_3.helper.GetAccountDetailsMethod;
import com.ecwid.mailchimp.method.v2_0.lists.ListMethod;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.jahia.modules.mailchimp.api.helpers.AccountDetailsMethod;

/**
 * Created by naf on 27.03.2015.
 */
public class Tools {

    public static MailChimpMethod getMethodByName(String methodName) {
        if ("list".equals(methodName))
            return new ListMethod();
        if ("GetAccountDetails".equals(methodName))
            return new GetAccountDetailsMethod();
        if ("helper/account-details".equals(methodName))
            return new AccountDetailsMethod();
        throw new IllegalArgumentException("Unknown method "+methodName);
    }

    public static JsonElement doMailChimpAction(String apiKey, String methodName) throws IOException, MailChimpException {
        MailChimpMethod method = getMethodByName(methodName);
        method.apikey = apiKey;
        MailChimpClient mailChimpClient = new MailChimpClient(new HttpClientConnectionManager());
        final Gson gson = MailChimpGsonFactory.createGson();
        JsonElement res = gson.toJsonTree(mailChimpClient.execute(method));
        return res;
    }

}
