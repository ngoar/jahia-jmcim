/**
 * ==========================================================================================
 * =                   JAHIA'S DUAL LICENSING - IMPORTANT INFORMATION                       =
 * ==========================================================================================
 *
 *                                 http://www.jahia.com
 *
 *     Copyright (C) 2002-2016 Jahia Solutions Group SA. All rights reserved.
 *
 *     THIS FILE IS AVAILABLE UNDER TWO DIFFERENT LICENSES:
 *     1/GPL OR 2/JSEL
 *
 *     1/ GPL
 *     ==================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE GPL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *     2/ JSEL - Commercial and Supported Versions of the program
 *     ===================================================================================
 *
 *     IF YOU DECIDE TO CHOOSE THE JSEL LICENSE, YOU MUST COMPLY WITH THE FOLLOWING TERMS:
 *
 *     Alternatively, commercial and supported versions of the program - also known as
 *     Enterprise Distributions - must be used in accordance with the terms and conditions
 *     contained in a separate written agreement between you and Jahia Solutions Group SA.
 *
 *     If you are unsure which license is appropriate for your use,
 *     please contact the sales department at sales@jahia.com.
 */
package org.jahia.modules.mailchimp.api.campaigns.list;

import com.ecwid.mailchimp.MailChimpObject;
import org.jahia.modules.mailchimp.api.campaigns.create.TypeOpts;

import java.util.List;

/**
 * Created by Alexander Storozhuk on 05.05.2015.
 * Time: 16:16
 */
public class Data extends MailChimpObject {

    @Field
    public String id;
    @Field
    public Integer web_id;
    @Field
    public String list_id;
    @Field
    public Integer folder_id;
    @Field
    public Integer template_id;
    @Field
    public String content_type;
    @Field
    public String title;
    @Field
    public String type;
    @Field
    public String create_time;
    @Field
    public String send_time;
    @Field
    public Integer emails_sent;
    @Field
    public String status;
    @Field
    public String from_name;
    @Field
    public String from_email;
    @Field
    public String subject;
    @Field
    public String to_name;
    @Field
    public String archive_url;
    @Field
    public Boolean inline_css;
    @Field
    public String analytics;
    @Field
    public String analytics_tag;
    @Field
    public Boolean authenticate;
    @Field
    public Boolean ecomm360;
    @Field
    public Boolean auto_tweet;
    @Field
    public String auto_fb_post;
    @Field
    public Boolean auto_footer;
    @Field
    public Boolean timewarp;
    @Field
    public String timewarp_schedule;
    @Field
    public String parent_id;
    @Field
    public Boolean is_child;
    @Field
    public String tests_sent;
    @Field
    public Integer tests_remain;
    @Field
    public Tracking tracking;
    @Field
    public String segment_text;
    @Field
    public List<MailChimpObject> segment_opts;
    @Field
    public List<SavedSegment> saved_segment;
    @Field
    public List<TypeOpts> type_opts;
    @Field
    public Integer comments_total;
    @Field
    public Integer comments_unread;
    @Field
    public SocialCard social_card;

}
