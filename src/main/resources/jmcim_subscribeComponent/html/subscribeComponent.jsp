<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="target" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<c:set var="target" value="${currentNode.properties['j:target']==null ? null : currentNode.properties['j:target'].node}"/>
<c:set value="${jmcim:settings(renderContext.mainResource.node)}" var="mailchimp" scope="request"/>
<c:if test="${!renderContext.liveMode}">
    <c:choose>
        <c:when test="${empty target }">
            <p><fmt:message key="jmcim.subscriptions.noTarget"/></p>
        </c:when>
        <c:when test="${!mailchimp.active}">
            <p><fmt:message key="jmcim.subscriptions.notActive"/></p>
        </c:when>
        <c:otherwise>
            <p>${fn:escapeXml(functions:default(currentNode.propertiesAsString['jcr:title'], target.displayableName))}&nbsp;-&nbsp;<fmt:message
                    key="jmcim.liveModeOnly"/></p>
        </c:otherwise>
    </c:choose>
</c:if>
<c:if test="${renderContext.liveMode && not empty target && mailchimp.active}">
    <%-- Subscriptions are available in live mode only --%>
    <template:addResources type="css" resources="jquery.fancybox.css"/>
    <template:addResources type="css" resources="jahia.fancybox-form.css"/>
    <template:addResources type="javascript" resources="jquery.min.js,jquery-ui.min.js,jquery.fancybox.js"/>

    <c:set var="UID" value="${fn:replace(currentNode.identifier, '-', '_')}"/>
    <c:set var="subscribeTitle"
           value="${fn:escapeXml(functions:default(currentNode.propertiesAsString['jcr:title'], target.displayableName))}"/>
    <c:set var="unsubscribe_block">
        <a href="#" onclick="jmcimSubscribe_${UID}(false); return false;" title="<fmt:message key='jmcim.unsubscribe'/>">
            <img src="<c:url value='${url.currentModule}/icons/unsubscribe.png'/>" height="16" width="16"/>
        </a>
    </c:set>
    <c:set var="subscribe_block">
        <a href="#" onclick="jmcimSubscribe_${UID}(true); return false;" title="<fmt:message key='jmcim.subscribe'/>">
            <img src="<c:url value='${url.currentModule}/icons/subscribe.png'/>" height="16" width="16"/>
        </a>
    </c:set>

        <script type="text/javascript">
            function jmcimSubscribe_${UID}(subscribe) {
                var url = '<c:url value="${url.base}${currentNode.path}"/>'+(subscribe ? '.jmcimSubscribe' : '.jmcimUnsubscribe')+'.do';
                $.fancybox.showLoading();
                $.ajax({
                           type: "POST",
                           url: url,
                           cache: false,
                           success: function (data, textStatus, xhr) {
                               $.fancybox.hideLoading();
                               if (data && data.msg) {
                                   alert(data.msg);
                               }
                               if (data && data.status=='ok') {
                                   if (subscribe)
                                       $('#subscr_${UID}').html('${functions:escapeJavaScript( unsubscribe_block)}');
                                   else
                                       $('#subscr_${UID}').html('${functions:escapeJavaScript( subscribe_block)}');
                               }
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               $.fancybox.hideLoading();
                               if (xhr.status==401) {
                                   <fmt:message key="label.httpUnauthorized" var="msg"/>
                                   alert("${functions:escapeJavaScript(msg)}");
                               } else {
                                   alert(xhr.status+": "+xhr.statusText);
                               }
                           },
                           dataType: "json"
                       });
            }
        </script>
    <p>${subscribeTitle}

    <div id="subscr_${UID}">
        <c:choose>
            <c:when test="${renderContext.loggedIn}">
                ${jmcim:hasSubscribed(target, renderContext.user) ? unsubscribe_block : subscribe_block}
            </c:when>
            <c:when test="${fn:length(currentNode.properties['code'].string)>0}">
                ${currentNode.properties['code'].string}
            </c:when>
            <c:when test="${currentNode.properties['j:popup'].boolean}">
                <div id="maichimp-subscribe-form" style="display: none">
                    <template:addResources>
                    <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js"data-dojo-config="usePlainJson: true, isDebug: false"></script>
                    <script type="text/javascript">
//                        var doForm=false;
                        function doMailchimpPopup(prefix,uuid,lid){
//                            if(!doForm){
//                                doForm=true;
                            $.fancybox.showLoading();
                            require(["mojo/signup-forms/Loader"],
                                    function(L) {
                                        L.start({"baseUrl":"mc."+prefix+"list-manage.com","uuid":uuid,"lid":lid});
                                        setTimeout(function(){
                                            $(".mc-modal").attr("style","display: block; opacity: 1;");
                                            $(".mc-modal-bg").attr("style","display: block; opacity: 0.1;");
                                            $.fancybox.hideLoading();
                                        },1000);
                                    });
//                            }
                        };
                    </script>
                    </template:addResources>
                </div>
                <a class="openform" href="" onclick="doMailchimpPopup('${mailchimp.apiPrefix}','${mailchimp.accoutUuid}','${target.name}');return false;" title="<fmt:message key='jmcim.subscribe'/>">
                    <img src="<c:url value='${url.currentModule}/icons/subscribe.png'/>"
                         alt="<fmt:message key='jmcim.subscribe'/>" title="<fmt:message key='jmcim.subscribe'/>" height="16" width="16"/>
                </a>
            </c:when>
            <c:otherwise>
                <a href="${target.propertiesAsString['subscribeUrlShort']}" target="_blank"
                   onclick="open('${target.propertiesAsString['subscribeUrlShort']}','','',false);return false;"
                   title="<fmt:message key='jmcim.subscribe'/>">
                    <img src="<c:url value='${url.currentModule}/icons/subscribe.png'/>"
                         alt="<fmt:message key='jmcim.subscribe'/>" title="<fmt:message key='jmcim.subscribe'/>" height="16"
                         width="16"/></a>
            </c:otherwise>
        </c:choose>
    </div>
    </p>
</c:if>
