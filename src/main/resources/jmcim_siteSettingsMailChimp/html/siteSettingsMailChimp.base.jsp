<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>



<template:addResources type="css" resources="admin-bootstrap.css"/>
<template:addResources type="css" resources="datatables/css/bootstrap-theme.css"/>
<template:addResources type="css" resources="jquery.fancybox.css"/>
<template:addResources type="css" resources="jahia.fancybox-form.css"/>

<template:addResources type="javascript" resources="jquery.min.js"/>
<template:addResources type="javascript" resources="admin-bootstrap.js"/>
<template:addResources type="javascript" resources="jquery-ui.min.js,jquery.blockUI.js,workInProgress.js,jquery.fancybox.js"/>
<template:addResources type="javascript" resources="datatables/jquery.dataTables.js,i18n/jquery.dataTables-${currentResource.locale}.js,datatables/dataTables.bootstrap-ext.js"/>
<c:set value="${jmcim:settings(renderContext.mainResource.node)}" var="mailchimp" scope="request"/>
<div class="clearfix">
    <h1 class="pull-left"><fmt:message key="jmcim_config.title"/> - ${renderContext.site.name}</h1>
<%--
    <img alt="" src="<c:url value="${url.currentModule}/icons/refresh.png"/>"
         width="32" height="32" onclick="checkJmcimApiKey(true,intervalValue)">
--%>

    <div class="pull-right">
        <img alt="" src="<c:url value="${url.currentModule}/icons/logo_newscript_dark.png"/>"
             width="178" height="47">
    </div>
</div>

<template:area path="allcontent"/>
<div id="jmcimActiveContent">
<c:if test="${mailchimp.active}">
    <template:area path="activecontent"/>
</c:if>
</div>