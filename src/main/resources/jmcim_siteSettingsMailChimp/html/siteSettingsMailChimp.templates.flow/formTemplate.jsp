<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="user" uri="http://www.jahia.org/tags/user" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="var" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="mailSettings" type="org.jahia.services.mail.MailSettings"--%>
<%--@elvariable id="flowRequestContext" type="org.springframework.webflow.execution.RequestContext"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="template" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="newslettersRootNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
<%--@elvariable id="tm" type="org.jahia.modules.mailchimp.sitesettings.model.TemplateModel"--%>
<c:set value="${jmcim:settings(renderContext.mainResource.node)}" var="mailchimp" scope="request"/>

<template:addResources type="css" resources="newsletter-settings.css"/>

<fmt:message var="i18nNameMandatory" key="jmcim_template.errors.name.mandatory"/><c:set var="i18nNameMandatory" value="${fn:escapeXml(i18nNameMandatory)}"/>
<fmt:message var="i18nContentNodeMandatory"
             key="jmcim_template.errors.contentNode.mandatory"/><c:set var="i18nContentNodeMandatory" value="${fn:escapeXml(i18nContentNodeMandatory)}"/>
<fmt:message var="i18nCreationFailed" key="jmcim_template.errors.create.failed"/><c:set var="i18nCreationFailed" value="${fn:escapeXml(i18nCreationFailed)}"/>
<fmt:message var="i18nUpdateFailed" key="jmcim_template.errors.update.failed"/><c:set var="i18nUpdateFailed" value="${fn:escapeXml(i18nUpdateFailed)}"/>
<fmt:message var="i18nTemplateNamUniquer" key="jmcim_template.errors.name.unique"/><c:set var="i18nTemplateNamUniquer" value="${fn:escapeXml(i18nTemplateNamUniquer)}"/>
<fmt:message var="i18nMchipmTemplateNamUniquer" key="jmcim_template.errors.name.mchipmUnique"/><c:set var="i18nMchipmTemplateNamUniquer" value="${fn:escapeXml(i18nMchipmTemplateNamUniquer)}"/>


<c:set var="isUpdate" value="${tm.update}"/>
<c:set var="showRestore" value="${tm.showRestore}"/>


<template:addResources>
    <script type="text/javascript">
    function showTemplateErrors(messages, separator) {
        var message = "";
        for(var i = 0; i < messages.length; i++){
            if(i == 0){
                message = messages[i];
            }else {
                message += (separator + messages[i]);
            }
        }
        $("#templateFormErrorMessages").html(message);
        $("#templateFormErrorContainer").show();
    }

    function hideTemplateErrors() {
        $("#templateFormErrorMessages").empty();
        $("#templateFormErrorContainer").hide();
    }

    function submitTemplateForm(act) {
        $('#templateFormAction').val(act);
        $('#templateForm').submit();
    }

    $(document).ready(function() {
        $("#templateFormErrorClose").bind("click", function(){
            hideTemplateErrors();
        });
        $('#templateForm').submit(function () {
            if ($('#templateFormAction').val()=='cancel')
                return true;
            // validate fields
            var messages = [];
            if (!$("#templateName").val().trim()) {
                messages.push("${i18nNameMandatory}");
            }
            if (!$("#contentNode").val()) {
                messages.push("${i18nContentNodeMandatory}");
            }
            if (messages.length>0) {
                showTemplateErrors(messages, "</br>");
                return false;
            }
            $.fancybox.showLoading();
            return true;
        })
    });

    </script>
</template:addResources>

<div>
    <c:choose>
        <c:when test="${isUpdate}">
            <h2><fmt:message key="jmcim_template.edit"/> - ${fn:escapeXml(tm.title)}</h2>
        </c:when>
        <c:otherwise>
            <h2><fmt:message key="jmcim_template.add"/></h2>
        </c:otherwise>
    </c:choose>

    <div class="box-1">
        <div class="alert alert-error" style="display: none" id="templateFormErrorContainer">
            <button type="button" class="close" id="templateFormErrorClose">&times;</button>
            <span id="templateFormErrorMessages"></span>
        </div>
        <c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
            <c:if test="${message.severity eq 'INFO'}">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        ${message.text}
                </div>
            </c:if>
            <c:if test="${message.severity eq 'ERROR'}">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        ${message.text}
                </div>
            </c:if>
        </c:forEach>
        <form action="${flowExecutionUrl}" method="POST" id="templateForm">
            <input type="hidden" name="_eventId" id="templateFormAction" value="ok"/>
            <input type="hidden" name="templateUUID" value="${templateUUID}"/>
            <fieldset>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <label for="templateName"><fmt:message key="label.name"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="tm.title" id="templateName" />
                        </div>
                    </div>
                    <c:if test="${showRestore}">
                        <div class="row-fluid">
                            <div class="span12">
                                <form:checkbox path="tm.restore"/>&nbsp;<fmt:message key="jmcim_template.restore"/>
                            </div>
                        </div>
                    </c:if>
                    <div class="row-fluid">
                        <div class="span12">
                            <label for="contentNode"><fmt:message key="jmcim_template.contentNode"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input disabled="true" path="tm.contentNodeDecoy" type="text" id="contentNodeDecoy" name="contentNodeDecoy" class="left"/>
                            <div class="left picker">
                                <form:input path="tm.contentNode" type="hidden" id="contentNode"/>
                                <ui:treeItemSelector fieldId="contentNode" displayFieldId="contentNodeDecoy"
                                                     displayIncludeChildren="false" valueType="identifier"
                                                     selectableNodeTypes="jmix:hasTemplateNode"
                                                     nodeTypes="jmix:hasTemplateNode,jmix:visibleInPagesTree"/>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <button class="btn btn-primary" id="saveTemplateSettings" >
                                <i class="icon-${isUpdate ? 'share' : 'plus'} icon-white"></i>
                                &nbsp;<fmt:message key="label.${isUpdate ? 'update' : 'add'}"/>
                            </button>
                            <button class="btn" onclick="submitTemplateForm('cancel'); return false;">
                                <i class="icon-ban-circle"></i>
                                &nbsp;<fmt:message key="label.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>