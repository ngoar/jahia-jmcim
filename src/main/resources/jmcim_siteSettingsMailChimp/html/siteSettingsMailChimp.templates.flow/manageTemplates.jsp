<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
<c:set value="${jmcim:settings(renderContext.mainResource.node)}" var="mailchimp" scope="request"/>

<template:addResources type="css" resources="admin-bootstrap.css"/>
<template:addResources type="css" resources="datatables/css/bootstrap-theme.css"/>
<template:addResources type="javascript" resources="jquery.min.js"/>
<template:addResources type="javascript" resources="admin-bootstrap.js"/>
<template:addResources type="javascript" resources="jquery-ui.min.js,jquery.blockUI.js,workInProgress.js"/>
<template:addResources type="javascript"
                       resources="datatables/jquery.dataTables.js,i18n/jquery.dataTables-${currentResource.locale}.js,datatables/dataTables.bootstrap-ext.js"/>
<template:addResources type="javascript" resources="moment.min.js"/>

<%@include file="../i18n.inc"%>

<template:addResources>
    <script type="text/javascript">
        function submitTemplateForm(act, templ) {
            $('#templateFormAction').val(act);
            if (templ) {
                $('#templateFormSelected').val(templ);
            }
            $.fancybox.showLoading();
            $('#templateForm').submit();
        }
/*        function hideTemplateErrors() {
            $("#templateFormErrorMessages").empty();
            $("#templateFormErrorContainer").hide();
        }

        $(document).ready(function () {
            $("#templateFormErrorClose").bind("click", function () {
                hideTemplateErrors();
            });
        });*/
    </script>
</template:addResources>


<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="templateForm">
    <input type="hidden" name="selectedTemplate" id="templateFormSelected"/>
   <%-- <input type="hidden" name="config" value="${jmcimSettings.identifier}"/>--%>
    <input type="hidden" name="_eventId" id="templateFormAction"/>
</form>

<h2><fmt:message key="jmcim_template.title"/> <a href="https://${mailchimp.apiPrefix}admin.mailchimp.com/templates/" target="_blank">MailChimp</a></h2>

<div class="box-1">
    <%@include file="../messages.inc" %>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th><fmt:message key="jmcim_template.Name"/></th>
            <th><fmt:message key="jmcim_template.contentNode"/></th>
            <th><fmt:message key="jmcim_template.createDate"/></th>
            <th width="220"><fmt:message key="label.actions"/> </th>
        </tr>
        </thead>
        <tbody>
        <c:set value="${jcr:getChildrenOfType(mailchimp.templatesNode,'jmcim:Template')}" var="templates"/>
        <c:if test="${empty templates }">
            <tr>
                <td colspan="7"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${templates}" var="template" varStatus="loopStatus">
            <c:set value="${template.properties['contentNode'].node}" var="contentNode"/>
            <tr>
                <td>${loopStatus.count}</td>
                <td>${template.properties['jcr:title'].string}</td>
                <td>${contentNode.displayableName}</td>
                <td><fmt:formatDate value="${template.properties['scheduled'].date.time}" pattern="yyyy-MM-dd HH:mm"/></td>
                <td>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitTemplateForm('editTemplate', '${template.identifier}');return false;">
                        <i class="icon-pencil"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditMailChimp}" href="https://${mailchimp.apiPrefix}admin.mailchimp.com/templates/design?tid=${template.properties['mailchimpId'].string}" target="_blank">
                        <i class="icon-share-alt"></i>
                    </a>
                    <a class="btn btn-small"
                       onclick="{submitTemplateForm('updateTemplateContent', '${template.identifier}');} return false;" href="#updateTemplateContent"
                       title="Update content" style="margin-bottom:0;">
                        <i class="icon-refresh"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitTemplateForm('removeTemplate', '${template.identifier}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="buttons">
        <button id="addTemplate" type="button" class="btn btn-primary"
                onclick="submitTemplateForm('createTemplate')"> <%--hideTemplateErrors();--%>
            <i class="icon-plus icon-white"> </i><fmt:message key="jmcim_template.add"/></button>
        <div class="clear"></div>
    </div>
</div>