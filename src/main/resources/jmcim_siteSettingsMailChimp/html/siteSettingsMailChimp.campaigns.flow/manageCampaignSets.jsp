<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>

<template:addResources type="css" resources="admin-bootstrap.css"/>
<template:addResources type="css" resources="datatables/css/bootstrap-theme.css"/>
<template:addResources type="javascript" resources="jquery.min.js"/>
<template:addResources type="javascript" resources="admin-bootstrap.js"/>
<template:addResources type="javascript" resources="jquery-ui.min.js,jquery.blockUI.js,workInProgress.js"/>
<template:addResources type="javascript"
                       resources="datatables/jquery.dataTables.js,i18n/jquery.dataTables-${currentResource.locale}.js,datatables/dataTables.bootstrap-ext.js"/>
<template:addResources type="javascript" resources="moment.min.js"/>

<%@include file="../i18n.inc"%>

<template:addResources>
    <script type="text/javascript">
        function submitCampaignSetForm(act, camp) {
            $('#campaignSetFormAction').val(act);
            if (camp) {
                $('#campaignSetFormSelected').val(camp);
            }
            $.fancybox.showLoading();
            $('#campaignSetForm').submit();
        }
    </script>
</template:addResources>

<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="campaignSetForm">
    <input type="hidden" name="selectedCampaignSet" id="campaignSetFormSelected"/>
    <input type="hidden" name="_eventId" id="campaignSetFormAction"/>
</form>


<h2><fmt:message key="jmcim_campaigns.title"/> <a href="https://${mailchimp.apiPrefix}admin.mailchimp.com/campaigns/" target="_blank">MailChimp</a></h2>
<div class="box-1">
    <%@include file="../messages.inc" %>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th><fmt:message key="jmcim_campaigns.Name"/></th>
            <th><fmt:message key="jmcim_campaigns.contentNode"/></th>
            <th>&nbsp;<fmt:message key="jmcim_campaigns.list"/>&nbsp;</th>
            <th><fmt:message key="jmcim_campaigns.scheduler"/></th>
            <th><fmt:message key="jmcim_campaigns.createDate"/></th>
            <th width="200"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:set value="${jcr:getChildrenOfType(mailchimp.campaignsNode,'jmcim:CampaignSet')}" var="campaigns"/>
        <c:if test="${empty campaigns }">
            <tr>
                <td colspan="7"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${campaigns}" var="campaign" varStatus="loopStatus">
            <c:set value="${campaign.properties['contentNode'].node}" var="contentNode"/>
            <c:set value="${campaign.properties['list'].node}" var="listNode"/>
            <tr>
                <td>${loopStatus.count}</td>
                <td><a title="${campaign.properties['jcr:title'].string}" href="#showCampaigns" onclick="submitCampaignSetForm('showCampaigns', '${campaign.identifier}');return false;">${campaign.properties['jcr:title'].string}</a></td>
                <td><a title="${i18nEditMode}-${contentNode.displayableName}" href="${url.baseEdit}${contentNode.path}.html">${contentNode.displayableName}</a></td>
                <td>${listNode.displayableName}</td>
                <td>${campaign.properties['scheduler'].string}</td>
                <td><fmt:formatDate value="${campaign.properties['jcr:created'].date.time}" pattern="yyyy-MM-dd HH:mm"/></td>
                <td width="30%">
                    <c:if test="${!campaign.properties['wasSend'].string}">
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitCampaignSetForm('editCampaignSet', '${campaign.identifier}');return false;">
                        <i class="icon-pencil"></i>
                    </a>
                    </c:if>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditMode}" href="#showCampaigns"
                       onclick="submitCampaignSetForm('showCampaigns', '${campaign.identifier}');return false;" >
                        <i class="icon-info-sign"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nCampaignCreate}" href="#createCampaign"
                       onclick="submitCampaignSetForm('createCampaign', '${campaign.identifier}');return false;">
                        <i class="icon-file"></i>
                    </a>

                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditMailChimp}" href="https://${mailchimp.apiPrefix}admin.mailchimp.com/campaigns/show?id=${campaign.properties['templateCampaignWebId'].string}" target="_blank">
                        <i class="icon-share-alt"></i>
                    </a>
                    <c:if test="${!campaign.properties['wasSend'].string}">
                    <a class="btn btn-small"
                       onclick="{submitCampaignSetForm('updateCampaignSetContent', '${campaign.identifier}');} return false;" href="#updateCampaignSetContent"
                       title="Update content" style="margin-bottom:0;">
                        <i class="icon-refresh"></i>
                    </a>
                    </c:if>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitCampaignSetForm('removeCampaignSet', '${campaign.identifier}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="buttons">
        <button id="addCampaign" type="button" class="btn btn-primary"
                onclick="submitCampaignSetForm('createCampaignSet');">
            <i class="icon-plus icon-white"> </i><fmt:message key="jmcim_campaigns.add"/></button>
        <div class="clear"></div>
    </div>
</div>