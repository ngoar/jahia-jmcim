<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="user" uri="http://www.jahia.org/tags/user" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="var" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="mailSettings" type="org.jahia.services.mail.MailSettings"--%>
<%--@elvariable id="flowRequestContext" type="org.springframework.webflow.execution.RequestContext"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="template" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="newslettersRootNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
<%--@elvariable id="csm" type="org.jahia.modules.mailchimp.sitesettings.model.CampaignSetModel"--%>
<c:set value="${jmcim:settings(renderContext.mainResource.node)}" var="mailchimp" scope="request"/>

<template:addResources type="css" resources="newsletter-settings.css"/>

<fmt:message var="i18nNameMandatory" key="jmcim_template.errors.name.mandatory"/><c:set var="i18nNameMandatory" value="${fn:escapeXml(i18nNameMandatory)}"/>
<fmt:message var="i18nContentNodeMandatory" key="jmcim_template.errors.contentNode.mandatory"/><c:set var="i18nContentNodeMandatory" value="${fn:escapeXml(i18nContentNodeMandatory)}"/>

<fmt:message var="i18nSubject" key="jmcim_campaigns.errors.subject"/><c:set var="i18nSubject" value="${fn:escapeXml(i18nSubject)}"/>
<fmt:message var="i18nFromEmail" key="jmcim_campaigns.errors.fromEmail"/><c:set var="i18nFromEmail" value="${fn:escapeXml(i18nFromEmail)}"/>
<fmt:message var="i18nFromName" key="jmcim_campaigns.errors.fromName"/><c:set var="i18nFromName" value="${fn:escapeXml(i18nFromName)}"/>
<fmt:message var="i18nToName" key="jmcim_campaigns.errors.toName"/><c:set var="i18nToName" value="${fn:escapeXml(i18nToName)}"/>
<fmt:message var="i18nList" key="jmcim_campaigns.errors.list"/><c:set var="i18nList" value="${fn:escapeXml(i18nList)}"/>



<c:set var="isUpdate" value="${csm.update}"/>


<template:addResources>
    <script type="text/javascript">
    function showCampaignSetErrors(messages, separator) {
        var message = "";
        for(var i = 0; i < messages.length; i++){
            if(i == 0){
                message = messages[i];
            }else {
                message += (separator + messages[i]);
            }
        }
        $("#campaignSetFormErrorMessages").html(message);
        $("#campaignSetFormErrorContainer").show();
    }

    function hideTemplateErrors() {
        $("#campaignSetFormErrorMessages").empty();
        $("#campaignSetFormErrorContainer").hide();
    }

    function submitCampaignSetForm(act) {
        $('#campaignSetFormAction').val(act);
        $('#campaignSetForm').submit();
    }

    $(document).ready(function() {
        $("#campaignSetFormErrorClose").bind("click", function(){
            hideTemplateErrors();
        });
        $('#campaignSetForm').submit(function () {
            if ($('#campaignSetFormAction').val()=='cancel')
                return true;
            // validate fields
            var messages = [];
            if (!$("#campaignSetTitle").val().trim()) {
                messages.push("${i18nNameMandatory}");
            }
            if (!$("#campaignSetSubject").val() && messages.length < 1) {
                messages.push("${i18nSubject}");
            }
            if (!$("#campaignSetFromEmail").val() && messages.length < 1) {
                messages.push("${i18nFromEmail}");
            }
            if (!$("#campaignSetFromName").val() && messages.length < 1) {
                messages.push("${i18nFromName}");
            }
            if (!$("#campaignSetToName").val() && messages.length < 1) {
                messages.push("${i18nToName}");
            }
            if (!$("#listNode").val() && messages.length < 1) {
                messages.push("${i18nList}");
            }
            if (!$("#contentNode").val() && messages.length < 1) {
                messages.push("${i18nContentNodeMandatory}");
            }



            if (messages.length>0) {
                showCampaignSetErrors(messages, "</br>");
                return false;
            }
            $.fancybox.showLoading();
            return true;
        })
    });

    </script>
</template:addResources>

<div>
    <c:choose>
        <c:when test="${isUpdate}">
            <h2><fmt:message key="jmcim_campaigns.edit"/> - ${fn:escapeXml(csm.title)}</h2>
        </c:when>
        <c:otherwise>
            <h2><fmt:message key="jmcim_campaigns.add"/></h2>
        </c:otherwise>
    </c:choose>

    <div class="box-1">
        <div class="alert alert-error" style="display: none" id="campaignSetFormErrorContainer">
            <button type="button" class="close" id="campaignSetFormErrorClose">&times;</button>
            <span id="campaignSetFormErrorMessages"></span>
        </div>
        <c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
            <c:if test="${message.severity eq 'INFO'}">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        ${message.text}
                </div>
            </c:if>
            <c:if test="${message.severity eq 'ERROR'}">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        ${message.text}
                </div>
            </c:if>
        </c:forEach>
        <form action="${flowExecutionUrl}" method="POST" id="campaignSetForm">
            <input type="hidden" name="_eventId" id="campaignSetFormAction" value="ok"/>
            <input type="hidden" name="campaignSetUUID" value="${campaignSetUUID}"/>
            <fieldset>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
                            <label for="campaignSetTitle"><fmt:message key="label.name"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="csm.title" id="campaignSetTitle" />
                        </div>
                        <div class="span6">
                            <label for="campaignSetSubject"><fmt:message key="jmcim_campaigns.subject"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="csm.subject" id="campaignSetSubject" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <label for="campaignSetFromEmail"><fmt:message key="jmcim_campaigns.fromEmail"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="csm.fromEmail" id="campaignSetFromEmail" />
                        </div>
                        <div class="span6">
                            <label for="campaignSetFromName"><fmt:message key="jmcim_campaigns.fromName"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="csm.fromName" id="campaignSetFromName" />
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <label for="campaignSetToName"><fmt:message key="jmcim_campaigns.toName"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input path="csm.toName" id="campaignSetToName" />
                        </div>
                        <div class="span6">
                            <label for="listNode"><fmt:message key="jmcim_campaigns.list"/> <span class="text-error"><strong>*</strong></span></label>
                            <c:if test="${csm.listId != null}">
                                <jcr:node var="listDecoy" uuid="${csm.listId}"/>
                            </c:if>
                            <input value="${listDecoy == null ? '' : listDecoy.displayableName}" disabled="true" type="text" id="listIdDecoy" class="left"/>
                            <div class="left picker">
                                <form:input path="csm.listId" type="hidden" id="listNode"/>
                                <ui:treeItemSelector fieldId="listNode" displayFieldId="listIdDecoy"
                                                     displayIncludeChildren="false" valueType="identifier"
                                                     selectableNodeTypes="jmcim:List"
                                                     nodeTypes="jmcim:List"
                                                     root="${mailchimp.listRootPath}" />
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <label for="contentNode"><fmt:message key="jmcim_campaigns.contentNode"/> <span class="text-error"><strong>*</strong></span></label>
                            <form:input disabled="true" path="csm.contentNodeDecoy" type="text" id="contentNodeDecoy" name="contentNodeDecoy" class="left"/>
                            <div class="left picker">
                                <form:input path="csm.contentNode" type="hidden" id="contentNode"/>
                                <ui:treeItemSelector fieldId="contentNode" displayFieldId="contentNodeDecoy"
                                                     displayIncludeChildren="false" valueType="identifier"
                                                     selectableNodeTypes="jmix:hasTemplateNode"
                                                     nodeTypes="jmix:hasTemplateNode,jmix:visibleInPagesTree"/>
                            </div>
                        </div>
                        <div class="span6">
                            <label for="campaignSetScheduler"><fmt:message key="jmcim_campaigns.scheduler.info"/> </label>
                            <form:input path="csm.scheduler" id="campaignSetScheduler" /><br/>
                            <form:checkbox path="csm.autoSend"/>&nbsp;<fmt:message key="jmcim_campaigns.autoSend"/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <label for="campaignSetCleanPeriod"><fmt:message
                                    key="jmcim_campaigns.scheduler.delete"/> </label>
                            <form:input path="csm.cleanPeriodStr" id="campaignSetCleanPeriod"/><br/>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset><br/>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <button class="btn btn-primary">
                                <i class="icon-${isUpdate ? 'share' : 'plus'} icon-white"></i>
                                &nbsp;<fmt:message key="label.${isUpdate ? 'update' : 'add'}"/>
                            </button>
                            <button class="btn" onclick="submitCampaignSetForm('cancel'); return false;">
                                <i class="icon-ban-circle"></i>
                                &nbsp;<fmt:message key="label.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>