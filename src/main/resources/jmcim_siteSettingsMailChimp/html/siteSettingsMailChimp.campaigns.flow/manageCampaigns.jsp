<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
<template:addResources type="css" resources="admin-bootstrap.css"/>
<template:addResources type="css" resources="datatables/css/bootstrap-theme.css"/>
<template:addResources type="javascript" resources="jquery.min.js"/>
<template:addResources type="javascript" resources="admin-bootstrap.js"/>
<template:addResources type="javascript" resources="jquery-ui.min.js,jquery.blockUI.js,workInProgress.js"/>
<template:addResources type="javascript"
                       resources="datatables/jquery.dataTables.js,i18n/jquery.dataTables-${currentResource.locale}.js,datatables/dataTables.bootstrap-ext.js"/>
<template:addResources type="javascript" resources="moment.min.js"/>



<%@include file="../i18n.inc"%>

<template:addResources>
    <script type="text/javascript">
        function submitCampaignForm(act, camp) {
            $('#campaignFormAction').val(act);
            if (camp) {
                $('#campaignFormSelected').val(camp);
            }
            $.fancybox.showLoading();
            $('#campaignForm').submit();
        }
    </script>
</template:addResources>

<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="campaignForm">
    <input type="hidden" name="selectedCampaign" id="campaignFormSelected"/>
    <input type="hidden" name="_eventId" id="campaignFormAction"/>
</form>

<div class="buttons">
    <button id="showCampaignSets" type="button" class="btn"
            onclick="submitCampaignForm('cancel');">
        <i class="icon-backward"> </i>&nbsp;<fmt:message key="jmcim_back"/>
    </button>
    <div class="clear"></div>
</div>

<div class="box-1">
    <%@include file="../messages.inc" %>

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.Name"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.Status"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.emailsSentCount"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.errorsCount"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.unsubscribes"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.hardBounces"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.softBounces"/></th>
            <th style="word-wrap: normal"><fmt:message key="jmcim_campaign.sendTime"/></th>
            <th width="115"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:set value="${cmDetails}" var="campaigns"/>
        <c:if test="${empty campaigns }">
            <tr>
                <td colspan="7"><fmt:message key="label.noItemFound"/></td>
            </tr>
        </c:if>
        <c:forEach items="${campaigns}" var="campaign" varStatus="loopStatus">
            <tr>
                <td>${loopStatus.count}</td>
                <td>${campaign.node.properties['jcr:title'].string}</td>
                <td>${campaign.dataInfo.status}</td>
                <td>${campaign.summary.emailsSent}</td>
                <td>${campaign.summary.syntaxErrors}</td>
                <td>${campaign.summary.unsubscribes}</td>
                <td>${campaign.summary.hardBounces}</td>
                <td>${campaign.summary.softBounces}</td>
                <td>${campaign.dataInfo.send_time}</td>
                <td>
                    <%--
                    --%>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditMailChimp}" href="https://${mailchimp.apiPrefix}admin.mailchimp.com/campaigns/show?id=${campaign.node.properties['webId'].string}" target="_blank">
                        <i class="icon-share-alt"></i>
                    </a>

                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}"
                       href="#delete"
                       onclick="if (confirm('${i18nRemoveConfirm}')) {submitCampaignForm('removeCampaign', '${campaign.node.identifier}');} return false;">
                        <i class="icon-remove icon-white"></i>
                    </a>
                        <c:if test="${campaign.dataInfo.status == 'save'}">
                        <a style="margin-bottom:0;" class="btn btn-small" title="${i18nCampaignSend}" href="#campaignSend"
                           onclick="submitCampaignForm('campaignSend', '${campaign.node.identifier}');return false;">
                            <i class="icon-inbox"></i>
                        </a>
                    </c:if>

                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>