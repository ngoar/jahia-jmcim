<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
<template:addResources type="css" resources="admin-bootstrap.css"/>
<template:addResources type="css" resources="datatables/css/bootstrap-theme.css"/>

<template:addResources type="javascript" resources="jquery.min.js"/>
<template:addResources type="javascript" resources="admin-bootstrap.js"/>
<template:addResources type="javascript" resources="jquery-ui.min.js,jquery.blockUI.js,workInProgress.js"/>
<template:addResources type="javascript"
                       resources="datatables/jquery.dataTables.js,i18n/jquery.dataTables-${currentResource.locale}.js,datatables/dataTables.bootstrap-ext.js"/>
<fmt:message var="i18nCantVerifyKey" key="jmcim_config.CantVerifyKey"/><c:set var="i18nCantVerifyKey"
                                                                              value="${fn:escapeXml(i18nNameMandatory)}"/>


<template:addResources>
    <script type="text/javascript">
        var timerKeyCheck;
        var img_base = "${url.currentModule}/icons/";

        function checkJmcimApiKey(key) {
            //Calling Action to chek
            var url = '<c:url value="${url.base}${renderContext.mainResource.node.path}.jmcimApiKeyValidate.do"/>';
            if (key!=undefined)
                url = url+'?apiKey='+key;
            $.ajax({
                       contentType: 'application/json',
                       dataType: 'json',
                       type: "POST",
                       url: url,
                       success: function (data, textStatus, xhr) {
                           if (typeof(data.error)!="undefined") {
                               setNotify('error', data.error);
                           } else {
                               setNotify('success');
                               location.reload();
                           }
                       },
                       error: function (data, textStatus, xhr) {
                           setNotify('error', '${i18nCantVerifyKey}');
                       }

                   });

        }
        var prevKeyValue = '${functions:escapeJavaScript(mailchimp.apiKey)}';
        function onAPIKeyChange(currValue) {
            newKeyValue = $("#jmcimApiKeyField").val();
            if (newKeyValue!=prevKeyValue) {
                prevKeyValue = newKeyValue;
                clearTimeout(timerKeyCheck);
                setNotify('checking');
                timerKeyCheck = setTimeout(function () {
                    checkJmcimApiKey(newKeyValue);
                }, 300);
            }
        }
        function setNotify(type, message) {
            $('#key-test-icon').attr("src", img_base+"notify-"+type+"-icon.png");
            if (message) {
                $("#api-key-message").html(message);
                $("#api-key-error").show();
            } else {
                $("#api-key-error").hide();
                $("#api-key-message").html('');
            }

            if (type=='error') {
                $('#jmcimActiveContent').html('');
                $('.api-key-tip').show();
            }

        }
        $(document).ready(function () {
            $('#jmcimApiKeyField').keyup(onAPIKeyChange);
            $('#jmcimApiKeyField').change(onAPIKeyChange);
        });
    </script>
</template:addResources>

<div class="box-1">
    <div class="row-fluid">
        <div class="span12">
            <form class="form-horizontal" name="jmcimParameters">
                <fieldset>
                    <legend><fmt:message key="jmcim_config.apimsg"/></legend>
                    <div class="control-group">
                        <label class="control-label"><fmt:message key="jmcim_config.apikey"/></label>

                        <div class="controls">
                            <input id="jmcimApiKeyField" name="apiKey" type="text"
                                   value="${mailchimp.apiKey}" lang="24"/>
                            <img id="key-test-icon" width="26" height="26"
                                 src="<c:url value="${url.currentModule}/icons/notify-${mailchimp.active?'success':'error'}-icon.png"/>">
                            <%--
                            <button id="saveJmcimSettings" type="button" class="btn btn-primary"
                                    onclick="checkJmcimApiKey(true,intervalValue)" disabled>
                                <fmt:message key="label.save"/>
                            </button>
                            --%>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="alert alert-error" style="display: none" id="api-key-error">
        <%--<button type="button" class="close" id="templateFormErrorClose">&times;</button>--%>
        <span id="api-key-message"></span>
    </div>
    <div class="alert alert-info api-key-tip" style="display:${mailchimp.active?'none':'block'}">
        <%--<button type="button" class="close">&times;</button>--%>
            <span><a target="_blank" href="https://admin.mailchimp.com/account/api/"><fmt:message
                    key="jmcim_config.get_apikey"/></a>
            </span>
    </div>
    <div class="alert alert-info api-key-tip" style="display:${mailchimp.active?'none':'block'}">
        <%--<button type="button" class="close">&times;</button>--%>
            <span><a target="_blank" href="https://login.mailchimp.com/signup"><fmt:message
                    key="jmcim_config.get_account"/></a>
            </span>
    </div>


</div>
