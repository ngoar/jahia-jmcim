
<fmt:message var="i18nRemoveConfirm" key="jmcim_campaign.delete.title"/><c:set var="i18nRemoveConfirm"
                                                                               value="${functions:escapeJavaScript(i18nRemoveConfirm)}"/>
<fmt:message var="i18nRemove" key="label.remove"/><c:set var="i18nRemove" value="${functions:escapeJavaScript(i18nRemove)}"/>
<fmt:message var="i18nEdit" key="label.edit"/><c:set var="i18nEdit" value="${functions:escapeJavaScript(i18nEdit)}"/>
<fmt:message var="i18nEditMode" key="jmcim.vewCampStatus"/><c:set var="i18nEditMode" value="${functions:escapeJavaScript(i18nEditMode)}"/>
<fmt:message var="i18nEditMailChimp" key="jmcim.editInMailChimp"/><c:set var="i18nEditMailChimp" value="${functions:escapeJavaScript(i18nEditMailChimp)}"/>
<fmt:message var="i18nCampaignSend" key="jmcim_campaign.Send"/><c:set var="i18nCampaignSend" value="${fn:escapeXml(i18nCampaignSend)}"/>
<fmt:message var="i18nCampaignCreate" key="jmcim_campaign.Create"/><c:set var="i18nCampaignCreate" value="${fn:escapeXml(i18nCampaignCreate)}"/>


