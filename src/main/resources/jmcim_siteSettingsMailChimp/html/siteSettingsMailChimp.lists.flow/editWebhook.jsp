<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>

<template:addResources>
    <script type="text/javascript">
        function submitForm(act, value) {
            $('#formAction').val(act);
            $('#webhookUrl').removeAttr("disabled");
            $('#flowForm').submit();
        }
    </script>
</template:addResources>
<form action="${flowExecutionUrl}" method="post" style="display: inline;" id="flowForm">
    <input type="hidden" name="selectedItem" value="${listID}"/>
    <input type="hidden" name="_eventId" id="formAction"/>
    <input type="hidden" name="selectedItemKey" id="selectedItemKey"/>

    <div class="box-1">
    <legend><fmt:message key="jmcim_webhook"/></legend>
    <%@include file="../messages.inc" %>
    <fieldset>
        <div class="control-group">
            <h2><fmt:message key="jmcim_webhook.j_url"/></h2>
            <div class="controls">
                <input id="webhookUrl" name="webhookurl" type="text"  lang="44"
                       <c:if test="${fn:length(webhook.url)>0}">
                       value="${webhook.url}" disabled="disabled"
                       </c:if>
                        />
            </div>
        </div>
    </fieldset>
    <fieldset>
        <h2><fmt:message key="jmcim_webhook.j_actions"/></h2>
        <label for="subscribe" class="checkbox">
            <input type="checkbox" name="subscribe" id="subscribe" <c:if test="${webhook.actions.subscribe}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_subscribe"/>
        </label>
        <div class="clear"></div>
        <label for="unsubscribe" class="checkbox">
            <input type="checkbox" name="unsubscribe" id="unsubscribe" <c:if test="${webhook.actions.unsubscribe}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_unsubscribe"/>
        </label>
        <div class="clear"></div>
        <label for="profile" class="checkbox">
            <input type="checkbox" name="profile" id="profile" <c:if test="${webhook.actions.profile}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_profile"/>
        </label>
        <div class="clear"></div>
        <label for="cleaned" class="checkbox">
            <input type="checkbox" name="cleaned" id="cleaned" <c:if test="${webhook.actions.cleaned}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_cleaned"/>
        </label>
        <div class="clear"></div>
        <label for="upemail" class="checkbox">
            <input type="checkbox" name="upemail" id="upemail" <c:if test="${webhook.actions.upemail}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_upemail"/>
        </label>
        <div class="clear"></div>
        <label for="campaign" class="checkbox">
            <input type="checkbox" name="campaign" id="campaign" <c:if test="${webhook.actions.campaign}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_actions.j_campaign"/>
        </label>
        <div class="clear"></div>
    </fieldset>
    <fieldset>
        <h2><fmt:message key="jmcim_webhook.j_sources"/></h2>
        <label for="user" class="checkbox">
            <input type="checkbox" name="user" id="user" <c:if test="${webhook.sources.user}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_sources.j_user"/>
        </label>
        <div class="clear"></div>
        <label for="admin" class="checkbox">
            <input type="checkbox" name="admin" id="admin" <c:if test="${webhook.sources.admin}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_sources.j_admin"/>
        </label>
        <div class="clear"></div>
        <label for="api" class="checkbox">
            <input type="checkbox" name="api" id="api" <c:if test="${webhook.sources.api}">checked="checked"</c:if>/>
            <fmt:message key="jmcim_webhook.j_sources.j_api"/>
        </label>
        <div class="clear"></div>
    </fieldset>
    <div class="buttons">

    <button id="saveWebhook" type="button" class="btn btn-primary"
                onclick="submitForm('saveWebhookAction');">
            <fmt:message key="label.save"/>
        </button>
        <button id="cancelWebhook" type="button" class="btn btn-primary"
            onclick="submitForm('backToList')">
        <fmt:message key="label.cancel"/>
    </button>
    </div>
</div>
</form>
