<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="jmcim" uri="http://www.jahia.org/tags/jmcim" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>

<%@include file="../i18n.inc"%>

    <c:set var="lists" value="${mailchimp.listsSync}"/>
    <template:addResources>
        <script type="text/javascript">
            function submitForm(act, value) {
                $.fancybox.showLoading();
                $('#formAction').val(act);
                if (value) {
                    $('#selectedItem').val(value);
                }
                $('#flowForm').submit();
            }
        </script>
    </template:addResources>
    <form action="${flowExecutionUrl}" method="post" style="display: inline;" id="flowForm">
        <input type="hidden" name="selectedItem" id="selectedItem"/>
        <input type="hidden" name="_eventId" id="formAction"/>
    </form>

    <h2><fmt:message key="jmcim_lists.manage_lists"/> <a href="https://${mailchimp.apiPrefix}admin.mailchimp.com/lists/" target="_blank">MailChimp</a></h2>

    <div class="row-fluid">
        <div class="span3">
            <div class="buttons">
                <button id="addMailChimpList" type="button" class="btn"
                        onclick="open('https://${mailchimp.apiPrefix}admin.mailchimp.com/lists/new-list/', '','', true)">
                    <i class="icon-plus"> </i><fmt:message key="jmcim_lists.create"/></button>
                <div class="clear"></div>
            </div>
        </div>
    </div>

<div class="box-1">
    <%@include file="../messages.inc" %>
    <fieldset>
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th width="3%">#</th>
            <th><fmt:message key="imcim_lists.name"/></th>
            <th><fmt:message key="imcim_lists.email_option"/></th>
            <th><fmt:message key="imcim_lists.rating"/></th>
            <th><fmt:message key="imcim_lists.member_count"/></th>
            <th><fmt:message key="imcim_lists.unsubscribe_count"/></th>
            <th><fmt:message key="imcim_lists.cleaned_count"/></th>
            <th width="25%"><fmt:message key="label.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${lists.local}" var="list" varStatus="loopStatus">
            <tr>
                <td>${loopStatus.index+1}</td>
                <td>${list.name}</td>
                <td>${list.email_type_option}</td>
                <td><fmt:formatNumber type="number" value="${list.list_rating}"/></td>
                <td><fmt:formatNumber type="number" value="${list.stats.member_count}"/></td>
                <td><fmt:formatNumber type="number" value="${list.stats.unsubscribe_count}"/></td>
                <td><fmt:formatNumber type="number" value="${list.stats.cleaned_count}"/></td>
                <td>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit"
                       onclick="submitForm('editListAction','${list.id}')">
                        <i class="icon-pencil"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEditMailChimp}" href="https://${mailchimp.apiPrefix}admin.mailchimp.com/lists/members/?id=${list.web_id}" target="_blank">
                        <i class="icon-share-alt"></i>
                    </a>
                    <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}" href="#delete" onclick="submitForm('removeListAction','${list.id}');">
                        <i class="icon-remove icon-white"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
        <button class="btn btn-primary" onclick="$('#updateSiteFormWrap').toggle(); return false;">
            <i class="icon-plus  icon-white"></i>&nbsp;&nbsp;<fmt:message key="jmcim_lists.add"/>
        </button>
    </fieldset>
</div>
    <div class="box-1"  id="updateSiteFormWrap" style="display: ${data.listIds==null? 'none' : 'block' };">
        <fieldset>
            <form id="updateSiteForm" action="${flowExecutionUrl}" onsubmit="$.fancybox.showLoading(); return true;"  method="post">
                <input type="hidden" name="_eventId" value="mapListAction"/>
                <form:select path="data.listIds" id="remote_lists" multiple="multiple" size="${lists.remoteSize+1}">
                    <c:forEach var="mcList" items="${lists.remote}">
                        <form:option value="${mcList.id}" >${mcList.name} (<fmt:formatNumber type="number" value="${mcList.stats.member_count}"/>)</form:option>
                    </c:forEach>
                </form:select>
                <button class="btn btn-primary" type="submit"><i class="icon-plus icon-white"></i></button>
            </form>
        </fieldset>
    </div>

