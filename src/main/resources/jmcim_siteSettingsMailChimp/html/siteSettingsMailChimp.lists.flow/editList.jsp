<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>

<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="list" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="flowExecutionUrl" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>
    <template:addResources>
        <script type="text/javascript">
            function submitForm(act, value) {
                $('#formAction').val(act);
                if (value) {
                    $('#selectedItemKey').val(value);
                }
                $.fancybox.showLoading();
                $('#flowForm').submit();
            }
            function suscribeFormPreview(currValue) {
                $("#embed-form-display").contents().find('html').html(currValue);
            }
        </script>
    </template:addResources>
    <form action="${flowExecutionUrl}" method="post" style="display: inline;" id="flowForm">
        <input type="hidden" name="selectedItem" value="${listID}"/>
        <input type="hidden" name="_eventId" id="formAction"/>
        <input type="hidden" name="selectedItemKey" id="selectedItemKey"/>
    </form>

<%--
    <div class="clearfix">
        <h1 class="pull-left"><fmt:message key="jmcim_lists.edit"/> - ${renderContext.site.name}</h1>
        <div class="pull-right">
            <img alt="" src="<c:url value="${url.currentModule}/icons/logo_newscript_dark.png"/>"
                 width="178" height="47">
        </div>
    </div>
--%>
    <jcr:node var="list" path="${mailchimp.listsNode.path}/${listID}"/>

    <div class="row-fluid">
        <div class="span3">
            <div class="buttons">
                <button id="addMailChimpList" type="button" class="btn"
                        onclick="submitForm('back')">
                    <i class="icon-backward"> </i>&nbsp;<fmt:message key="jmcim_back"/></button>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <h2>${list.displayableName}  - <fmt:message key="jmcim_lists.manage_list"/> <a href="https://${mailchimp.apiPrefix}admin.mailchimp.com/lists/members/?id=${listID}" target="_blank">MailChimp</a></h2>

    <div class="box-1">
        <%@include file="../messages.inc" %>

        <c:set var="welcome" value="${list.propertiesAsString['welcome']==null ? 'no' : list.propertiesAsString['welcome']}"/>
        <fieldset>
            <h2><fmt:message key="jmcim_lists.options"/></h2>
            <label for="noWelcome" class="checkbox">
                <input type="radio" name="doubleOptin" id="noWelcome" onclick="submitForm('noWelcome',$(this).attr('checked')!='checked');"
                     <c:if test="${welcome == 'no'}">checked="checked"</c:if>/>
                <fmt:message key="jmcim_lists.noWelcome"/>
            </label>
            <div class="clear"></div>
            <label for="confirmationWelcome" class="checkbox">
                <input type="radio" name="confirmation" id="confirmationWelcome" onclick="submitForm('confirmationWelcome',$(this).attr('checked')!='checked');"
                       <c:if test="${welcome == 'confirmation'}">checked="checked"</c:if>/>
                <fmt:message key="jmcim_lists.confirmationOptin"/>
            </label>
            <div class="clear"></div>
            <label for="sendWelcome" class="checkbox">
                <input type="radio" name="sendWelcome" id="sendWelcome" onclick="submitForm('sendWelcome',$(this).attr('checked')!='checked');"
                       <c:if test="${welcome == 'welcome'}">checked="checked"</c:if>/>
                <fmt:message key="jmcim_lists.sendWelcome"/>
            </label>
            <div class="clear"></div>
            <h2><fmt:message key="jmcim_lists.list_integration"/></h2>
            <label for="setWebhook" class="checkbox">
                <input type="checkbox" name="setWebhook" id="setWebhook" onclick="submitForm('setWebhook',$(this).attr('checked')!='checked');"
                       <c:if test="${not empty list.propertiesAsString['webhookUrl'] }">checked="checked"</c:if>/>
                <fmt:message key="jmcim_lists.setWebhook"/>
            </label>
            <div class="clear"></div>
    </fieldset>
    </div>
    <div class="box-1">
        <fieldset>
            <h2><fmt:message key="jmcim_lists.varFields"/></h2>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th width="3%">#</th>
                    <th><fmt:message key="jmcim_listsFields.name"/></th>
                    <th><fmt:message key="jmcim_listsFields.tag"/></th>
                    <th><fmt:message key="jmcim_listsFields.jahiaField"/></th>
                    <th width="25%"><fmt:message key="label.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${varFields.local}" var="field" varStatus="loopStatus">
                    <tr>
                        <td>${loopStatus.index+1}</td>
                        <td>${field.vars.name}</td>
                        <td>${field.key}</td>
                        <td>${field.value}</td>
                        <td><c:if test="${not field.vars.req}">
                            <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}" href="#delete" onclick="submitForm('remfieldAction','${field.key}');">
                                <i class="icon-remove icon-white"></i>
                            </a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <c:if test="${fn:length(varFields.remote) > 0 }">
            <button class="btn btn-primary" onclick="$('#addFieldFormWrap').toggle();return false;">
                <i class="icon-plus  icon-white"></i>&nbsp;&nbsp;<fmt:message key="jmcim_listsFields.add"/>
            </button>
            </c:if>
        </fieldset>
    </div>
    <div class="box-1"  id="addFieldFormWrap" style="display: none;">
        <fieldset>
            <form id="updateSiteForm" action="${flowExecutionUrl}" method="post">
                <input type="hidden" name="_eventId" id="addfieldAction" value="addfieldAction"/>
                <input type="hidden" name="selectedItem" value="${listID}"/>
                <div class="input-append">
                <select name="selectedItemKey" id="remote_lists">
                    <c:forEach items="${varFields.remote}" var="field" varStatus="loopStatus">
                        <option value="${field.vars.tag}">${field.vars.name}</option>
                    </c:forEach>
                </select>
                    <input type="text" name="selectedItemValue" id="newVarFieldValue" value="" size="10">
                    <button title="Add" class="btn btn-primary" type="button" id="addVarField" onclick="$('#updateSiteForm').submit();">
                        <i class="icon-plus icon-white"></i>
                    </button>
                </div>
               <%-- <button class="btn btn-primary" type="button" onclick="addLists();"><i class="icon-plus icon-white"></i></button>--%>
            </form>
        </fieldset>
    </div>
<div class="box-1">
    <fieldset>
        <h2><fmt:message key="jmcim_webhook"/></h2>
        <c:if test="${fn:length(webhooks)>0}">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th width="3%">#</th>
                <th><fmt:message key="jmcim_webhook.j_url"/></th>
                <th><fmt:message key="jmcim_webhook.j_actions"/></th>
                <th><fmt:message key="jmcim_webhook.j_sources"/></th>
                <th width="25%"><fmt:message key="label.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${webhooks}" var="webhook" varStatus="loopStatus">
                <tr>
                    <td>${loopStatus.index+1}</td>
                    <td>${webhook.url}</td>
                    <td><span title="${webhook.actionsHint}">${webhook.actionsStr}</span></td>
                    <td><span title="${webhook.sourcesHint}">${webhook.sourcesStr}</span></td>
                    <td>
                        <c:if test="${not webhook.special}">
                        <a style="margin-bottom:0;" class="btn btn-small" title="${i18nEdit}" href="#edit" onclick="submitForm('editWebhookAction','${webhook.url}')">
                            <i class="icon-pencil"></i>
                        </a>
                        <a style="margin-bottom:0;" class="btn btn-danger btn-small" title="${i18nRemove}" href="#delete" onclick="submitForm('remWebhookAction','${webhook.url}');">
                            <i class="icon-remove icon-white"></i>
                        </a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </c:if>
        <button class="btn btn-primary" onclick="submitForm('addWebhookAction')">
            <i class="icon-plus  icon-white"></i>&nbsp;&nbsp;<fmt:message key="jmcim_webhook.add"/>
        </button>
    </fieldset>
</div>
