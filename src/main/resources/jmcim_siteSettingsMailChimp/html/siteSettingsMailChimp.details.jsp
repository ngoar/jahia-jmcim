<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="uiComponents" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="mailchimp" type="org.jahia.modules.mailchimp.service.MailChimpSettings"--%>

<c:set var="accountDetails" value="${mailchimp.API.accountDetails}"/>
<template:addResources >
    <style>
        .form-horizontal .static-input{
            display: inline-block;
            margin-bottom: 0;
            vertical-align: middle;
        }
        .static-input {
            overflow: hidden;
            white-space: nowrap;
            cursor: not-allowed;
            font-size: 11px;
            height: 16px;
            line-height: 16px;
            padding: 5px 6px;
            font-weight: bold;
        }    </style>
</template:addResources>
<div class="box-1">
    <div class="row-fluid">
        <div class="span12">
            <div class="form-horizontal">
            <fieldset>
                <legend><h2><fmt:message key="jmcim.account.title"/></h2></legend>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.user_id"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out value="${accountDetails.user_id}" /></div>
                    </div>
                </div>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.name"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out value="${accountDetails.username}" /></div>
                    </div>    
                </div>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.fname"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out  value="${accountDetails.contact.fname}"/></div>
                    </div>
                </div>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.lname"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out value="${accountDetails.contact.lname}"/></div>
                    </div>
                </div>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.email"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out value="${accountDetails.contact.email}"/></div>
                    </div>
                </div>
                <div class="control-row">
                    <label class="control-label"><fmt:message key="jmcim.account.company"/>:</label>
                    <div class="controls">
                        <div class="static-input"><c:out value="${accountDetails.contact.company}"/></div>
                    </div>
                </div>
            </fieldset>
            </div>
        </div>
<%--        <div class="span4">
        </div>--%>
    </div>
</div>
